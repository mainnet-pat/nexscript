FROM debian:bookworm-slim

ARG TARGETPLATFORM

RUN groupadd -r rostrum && useradd -r -m -g rostrum rostrum

RUN set -ex \
  && apt-get update \
  && apt-get install -qq --no-install-recommends ca-certificates wget \
  && rm -rf /var/lib/apt/lists/*

ENV ROSTRUM_VERSION 9.1.0

# install rostrum binaries
RUN set -ex \
  && [ $TARGETPLATFORM = "linux/arm64" ] && platform="arm64" || platform="linux64" \
  && export ROSTRUM_URL=https://bitcoinunlimited.gitlab.io/rostrum/nightly/rostrum-nexa-linux-${platform}-${ROSTRUM_VERSION}-bfe0ac2.gz \
  && [ $TARGETPLATFORM = "linux/arm64" ] && sha256="42d634c555e40fa8caea0469a0859f980a407151a4d41dfcd9f20a63243415db" || sha256="e32e05fd63161f6f1fe717fca789448d2ee48e2017d3d4c6686b4222fe69497e" \
  && export ROSTRUM_SHA256="$sha256" \
  && echo "$TARGETPLATFORM" "$ROSTRUM_SHA256" "$ROSTRUM_URL" \
  && cd /tmp \
  && wget -qO rostrum.gz "$ROSTRUM_URL" \
  # && echo "$ROSTRUM_SHA256 rostrum.gz" | sha256sum -c - \
  && gunzip rostrum.gz \
  && chmod +x rostrum \
  && mv rostrum /usr/local/bin \
  && rm -rf /tmp/*

# create data directory
ENV ROSTRUM_DATA /data
RUN mkdir "$ROSTRUM_DATA" \
  && chown -R rostrum:rostrum "$ROSTRUM_DATA" \
  && ln -sfn "$ROSTRUM_DATA" /home/rostrum/.rostrum \
  && chown -h rostrum:rostrum /home/rostrum/.rostrum

VOLUME /data

CMD ["rostrum"]