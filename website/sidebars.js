module.exports = {
  docs: [
    {
      type: 'category',
      label: 'Basics',
      items: [
        'basics/about',
        // 'basics/about-bch',
        'basics/getting-started',
        'basics/cli',
      ],
    },
    {
      type: 'category',
      label: 'Language Description',
      items: [
        'language/contracts',
        'language/types',
        'language/functions',
        'language/globals',
        'language/artifacts',
        'language/grammar',
        'language/examples',
      ],
    },
    {
      type: 'category',
      label: 'JavaScript SDK',
      items: [
        'sdk/instantiation',
        'sdk/transactions',
        'sdk/examples',
      ],
    },
    {
      type: 'category',
      label: 'Guides',
      items: [
        {
          type: 'category',
          label: 'MCP Contracts',
          items: [
            'guides/mcp/mcp',
            'guides/mcp/details'
          ],
        },
        'guides/multiplex/multiplex',
        'guides/script-templates',
        'guides/covenants',
      ],
    },
    {
      type: 'category',
      label: 'Releases',
      items: [
        'releases/release-notes',
      ],
    }
  ],
};
