module.exports = {
  title: 'NexScript',
  tagline: 'Smart contracts for Nexa',
  url: 'https://nexscript.org',
  baseUrl: '/',
  favicon: 'img/favicon.ico',
  organizationName: 'NexScript',
  projectName: 'nexscript',
  themeConfig: {
    prism: {
      theme: require('prism-react-renderer/themes/nightOwlLight'),
      darkTheme: require('prism-react-renderer/themes/nightOwl'),
      additionalLanguages: ['solidity', 'antlr4'],
    },
    image: 'img/nexscript-logo.svg',
    navbar: {
      logo: {
        alt: 'NexScript',
        src: 'img/nexscript-logo.svg',
      },
      links: [
        {to: '/docs/basics/about', label: 'Docs', position: 'right'},
        {
          href: 'https://gitlab.com/otoplo/nexscript',
          label: 'GitHub',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'light',
      links: [
        {
          title: 'Docs',
          items: [
            {
              label: 'Getting Started',
              to: '/docs/basics/getting-started',
            },
            {
              label: 'Examples',
              to: '/docs/language/examples',
            },
          ],
        },
        {
          title: 'Community',
          items: [
            {
              label: 'Telegram',
              href: 'https://t.me/nexscript',
            },
          ],
        },
        {
          title: 'Built by',
          items: [
            {
              html: `
                <a href="https://otoplo.com" target="_blank">
                  <img src="/img/otoplo.svg" alt="Otoplo"
                       style="border-radius: 5px; max-height: 40px" />
                </a>
              `,
            },
          ],
        },
        {
          title: 'Funded by',
          items: [
            {
              html: `
                <a href="https://www.bitcoinunlimited.info/" target="_blank">
                  <img src="/img/BU.svg" alt="Bitcoin Unlimited"
                       style="border-radius: 5px; max-height: 30px" />
                </a>
              `,
            },
          ],
        },
      ],
      copyright: `This project is a fork of the original 
        <a href="https://cashscript.org" target="_blank">CashScript</a> project.`,
    },
    googleAnalytics: {
      trackingID: 'UA-26805430-6',
    },
    algolia: {
      apiKey: 'b0b29787f1ed36aa2c1057bf4f6e6dde',
      indexName: 'cashscript',
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          editUrl:
            'https://gitlab.com/otoplo/nexscript/edit/master/website/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
  plugins: [
    [
      '@docusaurus/plugin-client-redirects',
      {
        fromExtensions: ['html'],
        redirects: [
          { from: ['/docs', '/docs/about', '/docs/basics'], to: '/docs/basics/about'},
          { from: '/docs/language', to: '/docs/language/contracts' },
          { from: '/docs/sdk', to: '/docs/sdk/instantiation' },
          { from: '/docs/guides', to: '/docs/guides/covenants' },
          { from: '/docs/getting-started', to: '/docs/basics/getting-started' },
          { from: '/docs/examples', to: '/docs/language/examples' },
        ],
      },
    ],
  ],
};
