import {
  binToHex,
  deriveHdPath,
  deriveHdPrivateNodeFromSeed,
} from '@bitauth/libauth';
import bip39 from 'bip39';
import nexcore from 'nexcore-lib';
import { PriceOracle } from './PriceOracle.js';
import { Network } from '../../src/interfaces.js';
import { ElectrumNetworkProvider } from '../../src/index.js';

export const network = globalThis.process?.env?.JEST_WORKER_ID === undefined ? Network.MAINNET : Network.REGTEST;

const seed = await bip39.mnemonicToSeed('NexScript Tests');

const rootNode = deriveHdPrivateNodeFromSeed(seed, true);
const baseDerivationPath = "m/44'/145'/0'/0";

const aliceNode = deriveHdPath(rootNode, `${baseDerivationPath}/0`);
const bobNode = deriveHdPath(rootNode, `${baseDerivationPath}/1`);
if (typeof aliceNode === 'string') throw new Error();
if (typeof bobNode === 'string') throw new Error();

export const alicePriv = aliceNode.privateKey;
const alicePubNexa = nexcore.PrivateKey(binToHex(alicePriv)).publicKey;
export const alicePub = new Uint8Array(alicePubNexa.toBuffer());
const aliceAddressObj = alicePubNexa.toAddress(nexcore.Networks.regtest);
export const aliceAddress = aliceAddressObj.toString();
export const alicePkh = new Uint8Array(aliceAddressObj.hashBuffer).slice(4);

export const bobPriv = bobNode.privateKey;
const bobPubNexa = nexcore.PrivateKey(binToHex(bobPriv)).publicKey;
export const bobPub = new Uint8Array(bobPubNexa.toBuffer());
const bobAddressObj = bobPubNexa.toAddress(nexcore.Networks.regtest);
export const bobAddress = bobAddressObj.toString();
export const bobPkh = new Uint8Array(bobAddressObj.hashBuffer).slice(4);

export const oracle = new PriceOracle(bobPriv);
export const oraclePub = bobPub;

console.log(aliceAddress, bobAddress);

export const fund = async (address: string, satoshis: number): Promise<any> => {
  const provider = new ElectrumNetworkProvider();
  const utxos = await provider.getUtxos(aliceAddress);

  const tx = new nexcore.Transaction();
  const utxo = utxos.filter((val) => val.satoshis > satoshis && !val.token)[0];
  const txo = {
    txId: utxo.txid,
    outputIndex: utxo.vout,
    satoshis: Number(utxo.satoshis),
    address: utxo.address,
  };

  tx.from(txo);
  tx.to(address, satoshis, 1);
  tx.change(aliceAddress);

  const privateKey = nexcore.PrivateKey(binToHex(alicePriv));
  tx.sign(privateKey, nexcore.crypto.Signature.SIGHASH_NEXA_ALL, 'schnorr');
  return provider.sendRawTransaction(tx.serialize());
};

export const createGroup = async (
  address: string,
  options?: { ticker: string, name: string, docUrl?: string, docHash?: string, decimals?: number },
): Promise<string> => {
  const provider = new ElectrumNetworkProvider();
  const utxos = await provider.getUtxos(aliceAddress);

  const tx = new nexcore.Transaction();
  const utxo = utxos.filter((val) => !val.token && val.satoshis > 10000n && val.satoshis > 10000n)[0];
  const txo = {
    txId: utxo.txid,
    outputIndex: utxo.vout,
    satoshis: Number(utxo.satoshis),
    address: utxo.address,
  };

  let opReturn;
  if (options) {
    opReturn = nexcore.GroupToken.buildTokenDescScript(options.ticker, options.name,
      options.docUrl, options.docHash, options.decimals);
  }

  const inputOutpoint = Buffer.from(txo.txId, 'hex');

  const id = nexcore.GroupToken.findGroupId(inputOutpoint, opReturn?.toBuffer(),
    nexcore.GroupToken.authFlags.ACTIVE_FLAG_BITS);
  const amount = nexcore.GroupToken.getAmountBuffer(nexcore.GroupToken.authFlags.ACTIVE_FLAG_BITS | id.nonce);

  tx.from(txo);

  if (opReturn) {
    tx.addGroupData(opReturn);
  }

  tx.toGrouped(address, id.hashBuffer, amount);
  tx.change(aliceAddress);

  const privateKey = nexcore.PrivateKey(binToHex(alicePriv));
  tx.sign(privateKey, nexcore.crypto.Signature.SIGHASH_NEXA_ALL, 'schnorr');
  await provider.sendRawTransaction(tx.serialize());
  return id.hashBuffer.toString('hex');
};

export const mintFungible = async (
  address: string,
  groupId: string,
  amount: bigint,
  decimals: number = 0,
): Promise<any> => {
  const provider = new ElectrumNetworkProvider();
  const utxos = await provider.getUtxos(aliceAddress);

  const tx = new nexcore.Transaction();

  const authUtxo = utxos.filter((val) => val.token?.groupId === groupId && val.token?.amount < 0n)[0];
  if (!authUtxo) throw new Error('No auth utxos available for this operation.');
  const authTxo = {
    txId: authUtxo.txid,
    outputIndex: authUtxo.vout,
    satoshis: Number(authUtxo.satoshis),
    address: authUtxo.address,
  };

  const utxo = utxos.filter((val) => !val.token && val.satoshis > 10000n)[0];
  const txo = {
    txId: utxo.txid,
    outputIndex: utxo.vout,
    satoshis: Number(utxo.satoshis),
    address: utxo.address,
  };

  tx.from(authTxo).from(txo);

  tx.toGrouped(aliceAddress, Buffer.from(groupId, 'hex'), authUtxo.token!.amount);
  tx.toGrouped(address, Buffer.from(groupId, 'hex'), amount * BigInt(10 ** decimals));
  tx.change(aliceAddress);
  const privateKey = nexcore.PrivateKey(binToHex(alicePriv));
  tx.sign(privateKey, nexcore.crypto.Signature.SIGHASH_NEXA_ALL, 'schnorr');
  return provider.sendRawTransaction(tx.serialize());
};

export const mintNonFungible = async (
  address: string,
  groupId: string, // hex encoded
  groupData: string, // hex encoded
  amount: bigint = 1n,
): Promise<any> => {
  const provider = new ElectrumNetworkProvider();
  const utxos = await provider.getUtxos(aliceAddress);

  const tx = new nexcore.Transaction();

  const authUtxo = utxos.filter((val) => val.token?.groupId === groupId && val.token?.amount < 0n)[0];
  if (!authUtxo) throw new Error('No auth utxos available');
  const authTxo = {
    txId: authUtxo.txid,
    outputIndex: authUtxo.vout,
    satoshis: Number(authUtxo.satoshis),
    address: authUtxo.address,
  };

  const utxo = utxos.filter((val) => !val.token && val.satoshis > 10000n)[0];
  const txo = {
    txId: utxo.txid,
    outputIndex: utxo.vout,
    satoshis: Number(utxo.satoshis),
    address: utxo.address,
  };

  tx.from(authTxo).from(txo);

  tx.toGrouped(address, nexcore.GroupToken.generateSubgroupId(Buffer.from(groupId, 'hex'), Buffer.from(groupData, 'hex')), amount);
  tx.change(aliceAddress);
  const privateKey = nexcore.PrivateKey(binToHex(alicePriv));
  tx.sign(privateKey, nexcore.crypto.Signature.SIGHASH_NEXA_ALL, 'schnorr');
  return provider.sendRawTransaction(tx.serialize());
};

export const sendAuthority = async (
  address: string,
  groupId: string,
): Promise<any> => {
  const provider = new ElectrumNetworkProvider();
  const utxos = await provider.getUtxos(aliceAddress);

  const tx = new nexcore.Transaction();

  const authorityUtxo = utxos.filter((val) => val.token?.groupId === groupId && val.token.amount < 0n)[0];
  if (!authorityUtxo) {
    throw new Error('No suitable utxos found to send tokens');
  }

  const authorityTxo = {
    txId: authorityUtxo.txid,
    outputIndex: authorityUtxo.vout,
    satoshis: Number(authorityUtxo.satoshis),
    address: authorityUtxo.address,
  };

  const utxo = utxos.filter((val) => !val.token && val.satoshis > 10000n)[0];
  const txo = {
    txId: utxo.txid,
    outputIndex: utxo.vout,
    satoshis: Number(utxo.satoshis),
    address: utxo.address,
  };

  tx.from(authorityTxo).from(txo);

  tx.toGrouped(address, Buffer.from(groupId, 'hex'), authorityUtxo.token!.amount);
  tx.change(aliceAddress);

  const privateKey = nexcore.PrivateKey(binToHex(alicePriv));
  tx.sign(privateKey, nexcore.crypto.Signature.SIGHASH_NEXA_ALL, 'schnorr');
  return provider.sendRawTransaction(tx.serialize());
};

export const sendTokens = async (
  address: string,
  groupId: string,
  amount: bigint = 1n,
  decimals: number = 0,
): Promise<any> => {
  const provider = new ElectrumNetworkProvider();
  const utxos = await provider.getUtxos(aliceAddress);

  const tx = new nexcore.Transaction();

  const tokenUtxo = utxos.filter((val) => val.token?.groupId === groupId
    && val.token.amount >= amount * BigInt(10 ** decimals))[0];
  if (!tokenUtxo) {
    throw new Error('No suitable utxos found to send tokens');
  }

  const tokenTxo = {
    txId: tokenUtxo.txid,
    outputIndex: tokenUtxo.vout,
    satoshis: Number(tokenUtxo.satoshis),
    address: tokenUtxo.address,
  };

  const utxo = utxos.filter((val) => !val.token && val.satoshis > 10000n)[0];
  const txo = {
    txId: utxo.txid,
    outputIndex: utxo.vout,
    satoshis: Number(utxo.satoshis),
    address: utxo.address,
  };

  tx.from(tokenTxo).from(txo);

  const tokenChange = tokenUtxo.token!.amount - amount * BigInt(10 ** decimals);

  tx.toGrouped(address, Buffer.from(groupId, 'hex'), amount * BigInt(10 ** decimals));
  // token change
  if (tokenChange > 0n) {
    tx.toGrouped(aliceAddress, Buffer.from(groupId, 'hex'), tokenChange);
  }
  tx.change(aliceAddress);

  const privateKey = nexcore.PrivateKey(binToHex(alicePriv));
  tx.sign(privateKey, nexcore.crypto.Signature.SIGHASH_NEXA_ALL, 'schnorr');
  return provider.sendRawTransaction(tx.serialize());
};
