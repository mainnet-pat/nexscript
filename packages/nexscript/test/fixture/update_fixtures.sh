#! /usr/bin/env sh

DIR=$(dirname "$0")
cd $DIR

NEXC="../../../nexc/dist/nexc-cli.js"

find . -maxdepth 1 -name "*.nex" | while read fn; do
    echo node "$NEXC" -o "$(basename "$fn" .nex).json" "$fn"
    node "$NEXC" -o "$(basename "$fn" .nex).json" "$fn"
done
