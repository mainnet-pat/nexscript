import { SourceArtifact } from '@nexscript/utils';
import {
  Contract,
  ElectrumNetworkProvider,
} from '../../src/index.js';
import { fund } from '../fixture/vars.js';

describe('Visible contract parameters', () => {
  beforeAll(async () => {
  });

  it('should fail when first parameter is visible', async () => {
    // given
    const artifact = {
      contracts: [{
        contracts: [],
        contractName: 'Compare',
        constructorInputs: [
          { name: 'one', type: 'int', visible: true },
          { name: 'two', type: 'int', visible: false },
        ],
        abi: [{ name: 'compare', inputs: [] }],
        bytecode: 'OP_FROMALTSTACK OP_FROMALTSTACK OP_LESSTHAN OP_VERIFY',
      }],
      source: '\n'
        + '    pragma nexscript >=0.4.0;\n'
        + '\n'
        + '    contract Compare(int visible one, int two) {\n'
        + '        // Allow the recipient to claim their received money\n'
        + '        function compare() {\n'
        + '            require(one > two);\n'
        + '        }\n'
        + '    }',
      compiler: { name: 'nexc', version: '0.1.0' },
      updatedAt: '2023-08-06T13:39:30.381Z',
    } as SourceArtifact;

    const provider = new ElectrumNetworkProvider();
    const contract = new Contract(artifact, [20n, 1n], { provider });
    await fund(contract.address, 100000);

    // when
    const txPromise = contract.functions
      .compare()
      .to(contract.address, 90000n)
      .send();

    // then
    await expect(txPromise).rejects.toThrow();
  });
  it('should pass when all parameters are hidden', async () => {
    // given
    const artifact = {
      contracts: [{
        contracts: [],
        contractName: 'Compare',
        constructorInputs: [
          { name: 'one', type: 'int', visible: false },
          { name: 'two', type: 'int', visible: false },
        ],
        abi: [{ name: 'compare', inputs: [] }],
        bytecode: 'OP_FROMALTSTACK OP_FROMALTSTACK OP_LESSTHAN OP_VERIFY',
      }],
      source: '\n'
        + '    pragma nexscript >=0.4.0;\n'
        + '\n'
        + '    contract Compare(int one, int two) {\n'
        + '        // Allow the recipient to claim their received money\n'
        + '        function compare() {\n'
        + '            require(one > two);\n'
        + '        }\n'
        + '    }',
      compiler: { name: 'nexc', version: '0.1.0' },
      updatedAt: '2023-08-06T13:39:35.992Z',
    } as SourceArtifact;

    const provider = new ElectrumNetworkProvider();
    const contract = new Contract(artifact, [20n, 1n], { provider });
    await fund(contract.address, 100000);

    // when
    const txPromise = contract.functions
      .compare()
      .to(contract.address, 90000n)
      .send();

    // then
    await expect(txPromise).resolves.not.toThrow();
  });

  it('should pass when last parameter is visible', async () => {
    // given
    const artifact = {
      contracts: [{
        contracts: [],
        contractName: 'Compare',
        constructorInputs: [
          { name: 'one', type: 'int', visible: false },
          { name: 'two', type: 'int', visible: true },
        ],
        abi: [{ name: 'compare', inputs: [] }],
        bytecode: 'OP_FROMALTSTACK OP_FROMALTSTACK OP_LESSTHAN OP_VERIFY',
      }],
      source: '\n'
        + '    pragma nexscript >=0.4.0;\n'
        + '\n'
        + '    contract Compare(int one, int visible two) {\n'
        + '        // Allow the recipient to claim their received money\n'
        + '        function compare() {\n'
        + '            require(one > two);\n'
        + '        }\n'
        + '    }',
      compiler: { name: 'nexc', version: '0.1.0' },
      updatedAt: '2023-08-06T13:39:35.992Z',
    } as SourceArtifact;

    const provider = new ElectrumNetworkProvider();
    const contract = new Contract(artifact, [20n, 1n], { provider });
    await fund(contract.address, 100000);

    // when
    const txPromise = contract.functions
      .compare()
      .to(contract.address, 90000n)
      .send();

    // then
    await expect(txPromise).resolves.not.toThrow();
  });

  it('should pass if all parameters are visible', async () => {
    // given
    const artifact = {
      contracts: [{
        contracts: [],
        contractName: 'Compare',
        constructorInputs: [
          { name: 'one', type: 'int', visible: true },
          { name: 'two', type: 'int', visible: true },
        ],
        abi: [{ name: 'compare', inputs: [] }],
        bytecode: 'OP_FROMALTSTACK OP_FROMALTSTACK OP_LESSTHAN OP_VERIFY',
      }],
      source: '\n'
        + '    pragma nexscript >=0.4.0;\n'
        + '\n'
        + '    contract Compare(int visible one, int visible two) {\n'
        + '        // Allow the recipient to claim their received money\n'
        + '        function compare() {\n'
        + '            require(one > two);\n'
        + '        }\n'
        + '    }',
      compiler: { name: 'nexc', version: '0.1.0' },
      updatedAt: '2023-08-06T13:39:43.119Z',
    } as SourceArtifact;

    const provider = new ElectrumNetworkProvider();
    const contract = new Contract(artifact, [20n, 1n], { provider });
    await fund(contract.address, 100000);

    // when
    const txPromise = contract.functions
      .compare()
      .to(contract.address, 90000n)
      .send();

    // then
    await expect(txPromise).resolves.not.toThrow();
  });

  it('should pass if all parameters are visible and verify the sum', async () => {
    // given
    const artifact = {
      contracts: [{
        contracts: [],
        contractName: 'Compare',
        constructorInputs: [
          { name: 'one', type: 'int', visible: true },
          { name: 'two', type: 'int', visible: true },
        ],
        abi: [{ name: 'compare', inputs: [] }],
        bytecode: 'OP_FROMALTSTACK OP_FROMALTSTACK OP_ADD 15 OP_EQUALVERIFY',
      }],
      source: '\n'
        + '    pragma nexscript >=0.4.0;\n'
        + '\n'
        + '    contract Compare(int visible one, int visible two) {\n'
        + '        // Allow the recipient to claim their received money\n'
        + '        function compare() {\n'
        + '            require(one + two === 21);\n'
        + '        }\n'
        + '    }',
      compiler: { name: 'nexc', version: '0.1.0' },
      updatedAt: '2023-08-06T13:39:43.119Z',
    } as SourceArtifact;

    const provider = new ElectrumNetworkProvider();
    const contract = new Contract(artifact, [20n, 1n], { provider });
    await fund(contract.address, 100000);

    // when
    const txPromise = contract.functions
      .compare()
      .to(contract.address, 90000n)
      .send();

    // then
    await expect(txPromise).resolves.not.toThrow();
  });

  it('should pass if all parameters are hidden and verify the sum', async () => {
    // given
    const artifact = {
      contracts: [{
        contracts: [],
        contractName: 'Compare',
        constructorInputs: [
          { name: 'one', type: 'int', visible: false },
          { name: 'two', type: 'int', visible: false },
        ],
        abi: [{ name: 'compare', inputs: [] }],
        bytecode: 'OP_FROMALTSTACK OP_FROMALTSTACK OP_ADD 15 OP_EQUALVERIFY',
      }],
      source: '\n'
        + '    pragma nexscript >=0.4.0;\n'
        + '\n'
        + '    contract Compare(int one, int two) {\n'
        + '        // Allow the recipient to claim their received money\n'
        + '        function compare() {\n'
        + '            require(one + two === 21);\n'
        + '        }\n'
        + '    }',
      compiler: { name: 'nexc', version: '0.1.0' },
      updatedAt: '2023-08-06T13:39:43.119Z',
    } as SourceArtifact;

    const provider = new ElectrumNetworkProvider();
    const contract = new Contract(artifact, [20n, 1n], { provider });
    await fund(contract.address, 100000);

    // when
    const txPromise = contract.functions
      .compare()
      .to(contract.address, 90000n)
      .send();

    // then
    await expect(txPromise).resolves.not.toThrow();
  });

  it('should have no constraints and no visible params, use satisfier args and verify the sum', async () => {
    // given
    const artifact = {
      contracts: [{
        contracts: [],
        contractName: 'Compare',
        constructorInputs: [],
        abi: [{
          name: 'compare',
          inputs: [
            { name: 'one', type: 'int' },
            { name: 'two', type: 'int' },
          ],
        }],
        bytecode: 'OP_ADD 15 OP_EQUALVERIFY',
      }],
      source: '\n'
        + '    pragma nexscript >=0.4.0;\n'
        + '\n'
        + '    contract Compare() {\n'
        + '        // Allow the recipient to claim their received money\n'
        + '        function compare(int one, int two) {\n'
        + '            require(one + two === 21);\n'
        + '        }\n'
        + '    }',
      compiler: { name: 'nexc', version: '0.1.0' },
      updatedAt: '2023-08-06T13:39:43.119Z',
    } as SourceArtifact;

    const provider = new ElectrumNetworkProvider();
    const contract = new Contract(artifact, [], { provider });
    await fund(contract.address, 100000);

    // when
    const txPromise = contract.functions
      .compare(20n, 1n)
      .to(contract.address, 90000n)
      .send();

    // then
    await expect(txPromise).resolves.not.toThrow();
  });
});
