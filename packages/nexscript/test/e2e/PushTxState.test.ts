import { SourceArtifact } from '@nexscript/utils';
import {
  Contract,
  ElectrumNetworkProvider,
  SignatureTemplate,
} from '../../src/index.js';
import {
  aliceAddress, alicePriv, createGroup, fund, mintFungible, sendTokens,
} from '../fixture/vars.js';

describe('Tx state tests', () => {
  beforeAll(async () => {
  });

  // groupNthInput and groupNthOutput are not tested because they require spending group, which is not suppoerted yet
  it('should pass the lookups', async () => {
    // given
    const artifact = {
      contracts: [{
        contracts: [],
        contractName: 'test',
        constructorInputs: [
        ],
        abi: [{ name: 'test', inputs: [] }],
        bytecode: 'OP_2 OP_PUSH_TX_STATE OP_SIZE OP_NIP OP_0 OP_NUMNOTEQUAL OP_VERIFY OP_3 OP_PUSH_TX_STATE OP_SIZE OP_NIP OP_0 OP_NUMNOTEQUAL OP_VERIFY OP_5 OP_PUSH_TX_STATE OP_0 OP_NUMNOTEQUAL OP_VERIFY OP_6 OP_PUSH_TX_STATE OP_0 OP_NUMNOTEQUAL OP_VERIFY OP_7 0000000000000000000000000000000000000000000000000000000000000000 OP_CAT OP_PUSH_TX_STATE OP_0 OP_NUMEQUALVERIFY OP_8 0000000000000000000000000000000000000000000000000000000000000000 OP_CAT OP_PUSH_TX_STATE OP_0 OP_NUMEQUALVERIFY OP_9 0000000000000000000000000000000000000000000000000000000000000000 OP_CAT OP_PUSH_TX_STATE OP_0 OP_NUMEQUALVERIFY OP_10 0000000000000000000000000000000000000000000000000000000000000000 OP_CAT OP_PUSH_TX_STATE OP_0 OP_NUMEQUALVERIFY',
      }],
      source: `contract test() {
        function test() {
            require(tx.id.length != 0);
            require(tx.idem.length != 0);
            require(tx.amountIn != 0);
            require(tx.amountOut != 0);
            require(tx.groupAmountIn(0x0000000000000000000000000000000000000000000000000000000000000000) == 0);
            require(tx.groupAmountOut(0x0000000000000000000000000000000000000000000000000000000000000000) == 0);
            require(tx.groupCountIn(0x0000000000000000000000000000000000000000000000000000000000000000) == 0);
            require(tx.groupCountOut(0x0000000000000000000000000000000000000000000000000000000000000000) == 0);
        }
      }`,
      compiler: { name: 'nexc', version: '0.1.2' },
      updatedAt: '2023-08-13T07:44:01.189Z',
    } as SourceArtifact;

    const provider = new ElectrumNetworkProvider();
    const contract = new Contract(artifact, [], { provider });
    await fund(contract.address, 100000);

    // when
    const txPromise = contract.functions
      .test()
      .to(contract.address, 90000n)
      .send();

    // then
    await expect(txPromise).resolves.not.toThrow();
  });

  it('should create group, mint tokens and fund contract with tokens', async () => {
    const groupId = await createGroup(aliceAddress);
    await mintFungible(aliceAddress, groupId, 1000n);

    const artifact = {
      contracts: [{
        contracts: [],
        contractName: 'test',
        constructorInputs: [],
        abi: [
          {
            name: 'test',
            inputs: [
              {
                name: 'groupId',
                type: 'bytes32',
              },
            ],
          },
        ],
        bytecode: 'OP_3 OP_PUSH_TX_STATE OP_SIZE OP_NIP OP_0 OP_NUMNOTEQUAL OP_VERIFY OP_7 OP_OVER OP_CAT OP_PUSH_TX_STATE f401 OP_NUMEQUALVERIFY OP_8 OP_OVER OP_CAT OP_PUSH_TX_STATE f401 OP_NUMEQUALVERIFY OP_11 OP_0 OP_2 OP_NUM2BIN OP_CAT OP_OVER OP_CAT OP_PUSH_TX_STATE OP_0 OP_NUMEQUALVERIFY OP_12 OP_0 OP_2 OP_NUM2BIN OP_CAT OP_OVER OP_CAT OP_PUSH_TX_STATE OP_0 OP_NUMEQUALVERIFY OP_9 OP_OVER OP_CAT OP_PUSH_TX_STATE OP_1 OP_NUMEQUALVERIFY OP_10 OP_SWAP OP_CAT OP_PUSH_TX_STATE OP_1 OP_NUMEQUALVERIFY',
      }],
      source: `
        contract test() {
          function test(bytes32 groupId) {
            require(tx.idem.length != 0);
            require(tx.groupAmountIn(groupId) == 500);
            require(tx.groupAmountOut(groupId) == 500);
            require(tx.groupNthInput(0, groupId) == 0);
            require(tx.groupNthOutput(0, groupId) == 0);
            require(tx.groupCountIn(groupId) == 1);
            require(tx.groupCountOut(groupId) == 1);
        }
      }`,
      compiler: {
        name: 'nexc',
        version: '0.4.0',
      },
      updatedAt: '2023-08-31T09:28:26.054Z',
    } as SourceArtifact;
    const provider = new ElectrumNetworkProvider();
    const contract = new Contract(artifact, [], { provider });

    await sendTokens(contract.address, groupId, 500n);
    const aliceUtxo = (await provider.getUtxos(aliceAddress)).filter((val) => !val.token && val.satoshis > 10000n)[0];
    const utxos = (await contract.getUtxos()).filter((val) => val.token?.groupId === groupId);

    await contract.functions.test(groupId)
      .from(utxos[0])
      .fromP2PKT(aliceUtxo, new SignatureTemplate(alicePriv))
      .to([{
        amount: 546n,
        to: aliceAddress,
        token: {
          amount: 500n,
          groupId,
        },
      }])
      .send();
  });
});
