import { SourceArtifact } from '@nexscript/utils';
import {
  Contract, SignatureTemplate, ElectrumNetworkProvider,
} from '../../src/index.js';
import {
  alicePriv,
  alicePub,
  fund,
} from '../fixture/vars.js';
import { getTxOutputs } from '../test-util.js';
import { FailedTimeCheckError, Reason } from '../../src/Errors.js';

describe('HodlVault', () => {
  let artifact: SourceArtifact;

  beforeAll(async () => {
    artifact = {
      contracts: [{
        contracts: [],
        contractName: 'hodlVault',
        constructorInputs: [
          {
            name: 'ownerPk', type: 'pubkey', visible: false, unused: false,
          },
          {
            name: 'locktime', type: 'int', visible: true, unused: false,
          },
          {
            name: 'index', type: 'int', visible: true, unused: true,
          },
        ],
        abi: [{ name: 'spend', inputs: [{ name: 'ownerSig', type: 'sig' }] }],
        bytecode: 'OP_FROMALTSTACK OP_DROP OP_FROMALTSTACK OP_FROMALTSTACK OP_SWAP OP_CHECKLOCKTIMEVERIFY OP_DROP OP_CHECKSIGVERIFY',
      }],
      source: '\n'
        + '    contract hodlVault(pubkey ownerPk, int visible locktime, int visible unused index) {\n'
        + '      function spend(sig ownerSig) {\n'
        + '        require(tx.time >= locktime);\n'
        + '        require(checkSig(ownerSig, ownerPk));\n'
        + '      }\n'
        + '    }',
      compiler: { name: 'nexc', version: '0.3.0' },
      updatedAt: '2023-08-23T12:12:54.111Z',
    };
  });

  describe('send', () => {
    it('should succeed when locktime is satisfied', async () => {
      // given
      const provider = new ElectrumNetworkProvider();
      const hodlVault = new Contract(artifact, [alicePub, 100n, 1n], { provider });
      await fund(hodlVault.address, 100000);

      const to = hodlVault.address;
      const amount = 10000n;

      // when
      const tx = await hodlVault.functions
        .spend(new SignatureTemplate(alicePriv))
        .to(to, amount)
        .send();

      // then
      const txOutputs = getTxOutputs(tx);
      expect(txOutputs).toEqual(expect.arrayContaining([{ to, amount }]));
    });

    it('should fail when locktime is not satisfied', async () => {
      // given
      const provider = new ElectrumNetworkProvider();
      const hodlVault = new Contract(artifact, [alicePub, 10000n, 1n], { provider });
      await fund(hodlVault.address, 100000);

      const to = hodlVault.address;
      const amount = 10000n;

      // when
      const txPromise = hodlVault.functions
        .spend(new SignatureTemplate(alicePriv))
        .to(to, amount)
        .send();

      // then
      await expect(txPromise).rejects.toThrow(FailedTimeCheckError);
      await expect(txPromise).rejects.toThrow(Reason.UNSATISFIED_LOCKTIME);
    });
  });
});
