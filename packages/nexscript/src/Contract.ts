import { binToHex } from '@bitauth/libauth';
import {
  AbiFunction,
  asmToScript,
  calculateBytesize,
  ContractArtifact,
  ContractCreationParams,
  countOpcodes,
  generateRedeemScript,
  PrimitiveType,
  Script,
  scriptToBytecode,
  SourceArtifact,
} from '@nexscript/utils';
import { Transaction } from './Transaction.js';
import { Argument, encodeArgument } from './Argument.js';
import { ContractOptions, Utxo } from './interfaces.js';
import NetworkProvider from './network/NetworkProvider.js';
import {
  encodeArguments,
  replaceDependencyArgs,
  scriptTemplateToAddress, toGroupAddress,
} from './utils.js';
import { ElectrumNetworkProvider } from './network/index.js';

export class Contract {
  name: string;
  address: string;
  bytecode: string;
  bytesize: number;
  opcount: number;
  artifact: ContractArtifact;

  functions: {
    [name: string]: ContractFunction,
  };

  private redeemScript: Script;
  private templateScript: Script;
  private constraintScript: Script;
  private visibleArgs: Script = [];
  private dependencyArgs: { [contractName: string]: ContractCreationParams } = {};
  provider: NetworkProvider;

  /**
   * Create new contract instance
   * @param {SourceArtifact | ContractArtifact} artifact - Artifact to instantiate the contract. See @ref compileString
   * @param {Argument[]} constructorArgs - Contract constraint parameters
   * @param {NetworkProvider?} options.provider - Network provider for network interactions
   * @param {string?} options.contractName - if `artifact` is of type SourceArtifact, the name of contract to be instantiated
   * @param {ContractDependencies?} options.dependencyArgs - dependent contract creation parameters
   */
  constructor(
    artifact: SourceArtifact | ContractArtifact,
    constructorArgs: Argument[],
    private options?: ContractOptions,
  ) {
    this.provider = this.options?.provider ?? new ElectrumNetworkProvider();
    const contractName = options?.contractName;

    const isSourceArtifact = 'source' in artifact && 'contracts' in artifact;

    if (isSourceArtifact) {
      if (!artifact.contracts.length) {
        throw Error('Malformed artifact: empty contract list');
      }

      const contractArtifact = contractName
        ? artifact.contracts.find((contract) => contract.contractName === contractName)
        : artifact.contracts[0];
      if (!contractArtifact) {
        throw Error(`Contract '${contractName}' not found in artifact`);
      }
      this.artifact = contractArtifact;
    } else {
      this.artifact = artifact;
    }

    this.dependencyArgs = replaceDependencyArgs(this.artifact, options?.dependencyArgs);

    const expectedProperties = ['abi', 'bytecode', 'constructorInputs', 'contractName'];
    if (!expectedProperties.every((property) => property in this.artifact)) {
      throw new Error('Invalid or incomplete artifact provided');
    }

    const { encodedArgs, visibleArgs } = encodeArguments(
      constructorArgs,
      this.artifact.constructorInputs,
      this.artifact.contractName,
    );

    this.templateScript = asmToScript(this.artifact.bytecode);
    this.constraintScript = encodedArgs;
    this.visibleArgs = visibleArgs;

    this.redeemScript = generateRedeemScript(
      this.templateScript,
      encodedArgs as Uint8Array[],
    );

    // Populate the functions object with the contract's functions
    // (with a special case for single function, which has no "function selector")
    this.functions = {};
    if (this.artifact.abi.length === 1) {
      const f = this.artifact.abi[0];
      this.functions[f.name] = this.createFunction(f);
    } else {
      this.artifact.abi.forEach((f, i) => {
        this.functions[f.name] = this.createFunction(f, i);
      });
    }

    this.name = this.artifact.contractName;
    this.address = scriptTemplateToAddress(
      this.templateScript,
      this.constraintScript,
      this.visibleArgs,
      this.provider.network,
    );
    this.bytecode = binToHex(scriptToBytecode(this.templateScript));
    this.bytesize = calculateBytesize(this.templateScript);
    this.opcount = countOpcodes(this.templateScript);
  }

  tokenAddress(groupId: string, amount: bigint): string {
    return toGroupAddress(this.address, groupId, amount);
  }

  async getBalance(): Promise<bigint> {
    const utxos = await this.getUtxos();
    return utxos.reduce((acc, utxo) => acc + utxo.satoshis, 0n);
  }

  async getUtxos(): Promise<Utxo[]> {
    return this.provider.getUtxos(this.address);
  }

  private createFunction(abiFunction: AbiFunction, selector?: number): ContractFunction {
    return (...args: Argument[]) => {
      if (abiFunction.inputs.length !== args.length) {
        throw new Error(`Incorrect number of arguments passed to function ${abiFunction.name}`);
      }

      // Encode passed args (this also performs type checking)
      let encodedArgs = args
        .map((arg, i) => encodeArgument(arg, abiFunction.inputs[i].type));

      if (selector !== undefined) {
        encodedArgs = [encodeArgument(BigInt(selector), PrimitiveType.INT), ...encodedArgs];
      }

      return new Transaction(
        this.address,
        this.provider,
        this.redeemScript,
        abiFunction,
        encodedArgs,
        this.templateScript,
        this.constraintScript,
        this.visibleArgs,
      );
    };
  }
}

export type ContractFunction = (...args: Argument[]) => Transaction;
