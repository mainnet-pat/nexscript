import bip68 from 'bip68';
import {
  binToHex,
} from '@bitauth/libauth';
import delay from 'delay';
import {
  AbiFunction,
  Op,
  placeholder,
  Script,
  scriptToBytecode,
} from '@nexscript/utils';
import nexcore from 'nexcore-lib';
import deepEqual from 'fast-deep-equal';
import {
  Utxo,
  Output,
  Recipient,
  isSignableUtxo,
  TransactionDetails,
  SignableUtxo,
  NftObject,
  TokenDetails,
} from './interfaces.js';
import {
  meep,
  getInputSize,
  createOpReturnOutput,
  getTxSizeWithoutInputs,
  buildError,
  validateRecipient,
  utxoComparator,
  utxoTokenComparator,
  scriptTemplateToGroupLockingScript,
} from './utils.js';
import { DUST_LIMIT, p2stOutputSize } from './constants.js';
import NetworkProvider from './network/NetworkProvider.js';
import SignatureTemplate from './SignatureTemplate.js';

export class Transaction {
  private inputs: Utxo[] = [];
  private outputs: Output[] = [];

  private txid = '';

  private sequence = 0xfffffffe;
  private locktime: number;
  private feePerByte: number = 2.0;
  private hardcodedFee: bigint;
  private minChange: bigint = DUST_LIMIT;
  private tokenChange: boolean = true;

  constructor(
    private address: string,
    private provider: NetworkProvider,
    private redeemScript: Script,
    private abiFunction: AbiFunction,
    private args: (Uint8Array | SignatureTemplate)[],
    private templateScript: Script,
    private constraintScript: Script,
    private visibleArgs: Script,
  ) {}

  from(input: Utxo): this;
  from(inputs: Utxo[]): this;

  from(inputOrInputs: Utxo | Utxo[]): this {
    if (!Array.isArray(inputOrInputs)) {
      inputOrInputs = [inputOrInputs];
    }

    this.inputs = this.inputs.concat(inputOrInputs);

    return this;
  }

  fromP2PKT(input: Utxo, template: SignatureTemplate): this;
  fromP2PKT(inputs: Utxo[], template: SignatureTemplate): this;

  fromP2PKT(inputOrInputs: Utxo | Utxo[], template: SignatureTemplate): this {
    if (!Array.isArray(inputOrInputs)) {
      inputOrInputs = [inputOrInputs];
    }

    inputOrInputs = inputOrInputs.map((input) => ({ ...input, template }));

    this.inputs = this.inputs.concat(inputOrInputs);

    return this;
  }

  to(to: string, amount: bigint, token?: TokenDetails): this;
  to(outputs: Recipient[]): this;

  to(toOrOutputs: string | Recipient[], amount?: bigint, token?: TokenDetails): this {
    if (typeof toOrOutputs === 'string' && typeof amount === 'bigint') {
      const recipient = { to: toOrOutputs, amount, token };
      return this.to([recipient]);
    }

    if (Array.isArray(toOrOutputs) && amount === undefined) {
      toOrOutputs.forEach(validateRecipient);
      this.outputs = this.outputs.concat(toOrOutputs);
      return this;
    }
    throw new Error('Incorrect arguments passed to function \'to\'');
  }

  withOpReturn(chunks: string[]): this {
    this.outputs.push(createOpReturnOutput(chunks));
    return this;
  }

  withAge(age: number): this {
    this.sequence = bip68.encode({ blocks: age });
    return this;
  }

  withTime(time: number): this {
    this.locktime = time;
    return this;
  }

  withHardcodedFee(hardcodedFee: bigint): this {
    this.hardcodedFee = hardcodedFee;
    return this;
  }

  withFeePerByte(feePerByte: number): this {
    this.feePerByte = feePerByte;
    return this;
  }

  withMinChange(minChange: bigint): this {
    this.minChange = minChange;
    return this;
  }

  withoutChange(): this {
    return this.withMinChange(BigInt(Number.MAX_VALUE));
  }

  withoutTokenChange(): this {
    this.tokenChange = false;
    return this;
  }

  async build(): Promise<string> {
    this.locktime = this.locktime ?? await this.provider.getBlockHeight();
    await this.setInputsAndOutputs();

    const transaction = new nexcore.Transaction();

    this.outputs.forEach((output) => {
      if (output.to instanceof Uint8Array) {
        transaction.addOutput(new nexcore.Transaction.Output({
          type: 0,
          script: nexcore.Script(binToHex(output.to)),
          satoshis: 0,
        }));
      } else if (output.token) {
        transaction.toGrouped(output.to, Buffer.from(output.token.groupId, 'hex'), output.token.amount);
      } else {
        transaction.to(output.to, Number(output.amount), 1); // p2st
      }
    });

    // first add all inputs to complete transaction layout, then sign them in next loop
    this.inputs.forEach((utxo) => {
      // UTXO's with signature templates are signed using P2PKT
      const txo = {
        txId: utxo.txid,
        outputIndex: utxo.vout,
        satoshis: Number(utxo.satoshis),
        address: utxo.address,
      } as any;

      if (isSignableUtxo(utxo)) {
        transaction.from(txo);
        return;
      }

      // contract input
      const templateScript = nexcore.Script.fromHex(binToHex(scriptToBytecode(this.templateScript)));
      const constraintScript = this.constraintScript.length
        ? nexcore.Script.fromHex(binToHex(scriptToBytecode(this.constraintScript))) : nexcore.Opcode.OP_0;
      const visibleArgs = binToHex(scriptToBytecode(this.visibleArgs));

      if (utxo.token) {
        txo.script = scriptTemplateToGroupLockingScript(utxo.token.groupId, utxo.token.amount,
          this.templateScript, this.constraintScript, this.visibleArgs);
      }

      const encodedTokenAmount = utxo.token?.amount ? nexcore.GroupToken.getAmountBuffer(utxo.token!.amount) : undefined;

      const unspentOutput = new nexcore.Transaction.UnspentOutput(txo);
      const input = new nexcore.Transaction.Input.ScriptTemplate({
        groupId: utxo.token?.groupId,
        groupAmount: encodedTokenAmount,
        type: nexcore.Transaction.Input.DEFAULT_TYPE,
        output: new nexcore.Transaction.Output({
          script: unspentOutput.script,
          satoshis: unspentOutput.satoshis,
        }),
        prevTxId: unspentOutput.txId,
        outputIndex: unspentOutput.outputIndex,
        script: nexcore.Script.empty(),
        amount: unspentOutput.satoshis,
        templateScript,
        constraintScript,
        visibleArgs,
      }, templateScript, constraintScript, visibleArgs, undefined);
      transaction.addInput(input);
    });

    // sequences and locktime must be set before producing signatures
    if (this.locktime < 5e8) {
      transaction.lockUntilBlockHeight(this.locktime);
    } else {
      transaction.lockUntilDate(this.locktime);
    }

    this.inputs.forEach((utxo, index) => {
      if (isSignableUtxo(utxo)) {
        const { privateKey } = (utxo as SignableUtxo).template;
        const nexPrivateKey = nexcore.PrivateKey(binToHex(privateKey), this.provider.network);

        const signatures = transaction.inputs[index].getSignatures(transaction, nexPrivateKey, index,
          nexcore.crypto.Signature.SIGHASH_NEXA_ALL);
        transaction.inputs[index].addSignature(transaction, signatures[0], 'schnorr');
        return;
      }

      const completeArgs = this.args.slice().reverse().map((arg) => {
        if (!(arg instanceof SignatureTemplate)) return arg;

        const { privateKey } = arg as SignatureTemplate;
        const nexPrivateKey = nexcore.PrivateKey(binToHex(privateKey), this.provider.network);

        const signature = nexcore.Transaction.sighash.sign(transaction, nexPrivateKey,
          nexcore.crypto.Signature.SIGHASH_NEXA_ALL, index, transaction.inputs[index].templateScript);

        return Uint8Array.from([...signature.toDER()]);
      });

      const satisfierScript = completeArgs.length
        ? nexcore.Script.fromHex(binToHex(scriptToBytecode(completeArgs))) : nexcore.Script.empty();

      transaction.inputs[index].setScript(nexcore.Script.buildScriptTemplateIn(
        transaction.inputs[index].templateScript,
        transaction.inputs[index].constraintScript?.chunks?.length
          ? transaction.inputs[index].constraintScript : Op.OP_0,
        satisfierScript.toBuffer(),
      ));
    });

    const serialized = transaction.serialize();
    // rostrum issue. save txid for later query
    this.txid = transaction.id;
    return serialized;
  }

  async send(): Promise<TransactionDetails>;
  async send(raw: true): Promise<string>;

  async send(raw?: true): Promise<TransactionDetails | string> {
    const tx = await this.build();
    try {
      // rostrum issue. use txid and not the returned idem to get the transaction
      const { txid } = this;
      await this.provider.sendRawTransaction(tx);
      return raw ? await this.getTxDetails(txid, raw) : await this.getTxDetails(txid);
    } catch (e: any) {
      const reason = e.error ?? e.message;
      throw buildError(reason, meep(tx, this.inputs, this.redeemScript));
    }
  }

  private async getTxDetails(txid: string): Promise<TransactionDetails>;
  private async getTxDetails(txid: string, raw: true): Promise<string>;

  private async getTxDetails(txid: string, raw?: true): Promise<TransactionDetails | string> {
    for (let retries = 0; retries < 1200; retries += 1) {
      await delay(500);
      try {
        const hex = await this.provider.getRawTransaction(txid);

        if (raw) return hex;

        const nexcoreTransaction = nexcore.Transaction(hex);
        return { ...nexcoreTransaction, txid, hex };
      } catch (ignored) {
        // ignored
      }
    }

    // Should not happen
    throw new Error('Could not retrieve transaction details for over 10 minutes');
  }

  async meep(): Promise<string> {
    const tx = await this.build();
    return meep(tx, this.inputs, this.redeemScript);
  }

  private async setInputsAndOutputs(): Promise<void> {
    if (this.outputs.length === 0) {
      throw Error('Attempted to build a transaction without outputs');
    }

    const allUtxos = await this.provider.getUtxos(this.address);

    const tokenInputs = this.inputs.length > 0
      ? this.inputs.filter((input) => input.token)
      : selectAllTokenUtxos(allUtxos, this.outputs);

    // This throws if the manually selected inputs are not enough to cover the outputs
    if (this.inputs.length > 0) {
      selectAllTokenUtxos(this.inputs, this.outputs);
    }

    if (this.tokenChange) {
      const tokenChangeOutputs = createFungibleTokenChangeOutputs(tokenInputs, this.outputs, this.address);
      this.outputs.push(...tokenChangeOutputs);
    }

    // Construct list with all nfts in inputs
    const listNftsInputs: NftObject[] = [];
    // If inputs are manually selected, add their tokens to balance
    this.inputs.forEach((input) => {
      if (!input.token) return;
      if (input.token.groupId.length > 32 && !nexcore.GroupToken.isAuthority(input.token.amount)) {
        listNftsInputs.push({
          commitment: input.token.groupId.slice(32),
          groupId: input.token.groupId,
          amount: input.token.amount,
        });
      }
    });
    // Construct list with all nfts in outputs
    const listNftsOutputs: NftObject[] = [];
    // Subtract all token outputs from the token balances
    this.outputs.forEach((output) => {
      if (!output.token) return;
      if (output.token.groupId.length > 32 && !nexcore.GroupToken.isAuthority(output.token.amount)) {
        listNftsOutputs.push({
          commitment: output.token.groupId.slice(32),
          groupId: output.token.groupId,
          amount: output.token.amount,
        });
      }
    });
    // If inputs are manually provided, check token balances
    if (this.inputs.length > 0) {
      // Compare nfts in- and outputs, check if inputs have nfts corresponding to outputs
      // Keep list of nfts in inputs without matching output
      let unusedNfts = listNftsInputs;
      for (const nftInput of listNftsInputs) {
        for (let i = 0; i < listNftsOutputs.length; i += 1) {
          // Deep equality check token objects
          if (deepEqual(listNftsOutputs[i], nftInput)) {
            listNftsOutputs.splice(i, 1);
            unusedNfts = unusedNfts.filter((nft) => !deepEqual(nft, nftInput));
            break;
          }
        }
      }

      if (listNftsOutputs.length !== 0) {
        throw new Error(`NFT output with token groupId ${listNftsOutputs[0].groupId} does not have corresponding input`);
      }
      if (this.tokenChange) {
        for (const unusedNft of unusedNfts) {
          const tokenDetails: TokenDetails = {
            groupId: unusedNft.groupId,
            amount: unusedNft.amount,
          };
          const nftChangeOutput = { to: this.address, amount: BigInt(DUST_LIMIT), token: tokenDetails };
          this.outputs.push(nftChangeOutput);
        }
      }
    }

    // extra check to include auth in outputs to prevent implicit burning
    if (this.tokenChange) {
      tokenInputs
        .filter((input) => nexcore.GroupToken.isAuthority(input.token?.amount))
        .forEach((authInput) => {
          const hasAuthOutput = this.outputs.some((output) => output.token?.groupId === authInput.token!.groupId
            && output.token?.amount === authInput.token!.amount);
          if (!hasAuthOutput) {
            this.outputs.push({ to: this.address, amount: BigInt(DUST_LIMIT), token: authInput.token });
          }
        });
    }

    // Replace all SignatureTemplate with 65-length placeholder Uint8Arrays
    const placeholderArgs = this.args.map((arg) => (
      arg instanceof SignatureTemplate ? placeholder(65) : arg
    ));

    const templateScript = nexcore.Script.fromHex(binToHex(scriptToBytecode(this.templateScript)));
    const constraintScript = this.constraintScript.length
      ? nexcore.Script.fromHex(binToHex(scriptToBytecode(this.constraintScript))) : nexcore.Opcode.OP_0;
    const satisfierScript = placeholderArgs.length
      ? nexcore.Script.fromHex(binToHex(scriptToBytecode(placeholderArgs))) : nexcore.Script.empty();

    const scriptTemplateIn = nexcore.Script.buildScriptTemplateIn(
      templateScript,
      constraintScript.chunks?.length ? constraintScript : Op.OP_0,
      satisfierScript.toBuffer(),
    );

    // Add one extra byte per input to over-estimate tx-in count
    const contractInputSize = getInputSize(scriptTemplateIn.toBuffer()) + 1;

    // Note that we use the addPrecision function to add "decimal points" to BigInt numbers

    // Calculate amount to send and base fee (excluding additional fees per UTXO)
    let amount = addPrecision(this.outputs.reduce((acc, output) => acc + output.amount, 0n));
    let fee = addPrecision(this.hardcodedFee ?? getTxSizeWithoutInputs(this.outputs) * this.feePerByte);

    // Select and gather UTXOs and calculate fees and available funds
    let satsAvailable = 0n;
    if (this.inputs.length > 0) {
      // If inputs are already defined, the user provided the UTXOs and we perform no further UTXO selection
      if (!this.hardcodedFee) fee += addPrecision(this.inputs.length * contractInputSize * this.feePerByte);
      satsAvailable = addPrecision(this.inputs.reduce((acc, input) => acc + input.satoshis, 0n));
    } else {
      // If inputs are not defined yet, we retrieve the contract's UTXOs and perform selection
      const utxos = allUtxos.filter((utxo) => !utxo.token);

      // We sort the UTXOs mainly so there is consistent behaviour between network providers
      // even if they report UTXOs in a different order
      utxos.sort(utxoComparator).reverse();

      // Add all automatically added token inputs to the transaction
      for (const utxo of tokenInputs) {
        this.inputs.push(utxo);
        satsAvailable += addPrecision(utxo.satoshis);
        if (!this.hardcodedFee) fee += addPrecision(contractInputSize * this.feePerByte);
      }

      for (const utxo of utxos) {
        if (satsAvailable > amount + fee) break;
        this.inputs.push(utxo);
        satsAvailable += addPrecision(utxo.satoshis);
        if (!this.hardcodedFee) fee += addPrecision(contractInputSize * this.feePerByte);
      }
    }

    // Remove "decimal points" from BigInt numbers (rounding up for fee, down for others)
    satsAvailable = removePrecisionFloor(satsAvailable);
    amount = removePrecisionFloor(amount);
    fee = removePrecisionCeil(fee);

    // Calculate change and check available funds
    let change = satsAvailable - amount - fee;

    if (change < 0) {
      throw new Error(`Insufficient funds: available (${satsAvailable}) < needed (${amount + fee}).`);
    }

    // Account for the fee of adding a change output
    if (!this.hardcodedFee) {
      const outputSize = p2stOutputSize(this.address);
      change -= BigInt(outputSize * this.feePerByte);
    }

    // Add a change output if applicable
    if (change >= DUST_LIMIT && change >= this.minChange) {
      this.outputs.push({ to: this.address, amount: change });
    }
  }
}

const getTokenCategories = (outputs: Array<Output | Utxo>): string[] => (
  outputs
    .filter((output) => output.token)
    .map((output) => output.token!.groupId)
    .filter((value, index, array) => array.indexOf(value) === index) // unique only
);

const calculateTotalTokenAmount = (outputs: Array<Output | Utxo>, tokenCategory: string): bigint => (
  outputs
    .filter((output) => output.token?.groupId === tokenCategory)
    .filter((output) => output.token!.amount > 0n) // exclude authority utxo amounts
    .reduce((acc, output) => acc + output.token!.amount, 0n)
);

const selectTokenUtxos = (utxos: Utxo[], outputs: Output[], amountNeeded: bigint, tokenCategory: string): Utxo[] => {
  const tokenUtxos = utxos.filter((utxo) => utxo.token?.groupId === tokenCategory && utxo.token?.amount > 0n);

  // We sort the UTXOs mainly so there is consistent behaviour between network providers
  // even if they report UTXOs in a different order
  tokenUtxos.sort(utxoTokenComparator).reverse();

  let amountAvailable = 0n;
  const selectedUtxos: Utxo[] = [];

  // Add token UTXOs until we have enough to cover the amount needed (no fee calculation because it's a token)
  for (const utxo of tokenUtxos) {
    if (amountAvailable >= amountNeeded) break;

    // exclude authority utxos from amount counting
    if (utxo.token!.amount > 0n) {
      selectedUtxos.push(utxo);
      amountAvailable += utxo.token!.amount;
    }
  }

  if (amountAvailable === 0n) {
    // nft token creation
    const nftOutputs = outputs.filter((output) => output.token!.groupId.length > 64);
    if (nftOutputs.length) {
      const parentGroup = utxos.filter((input) => input.token?.groupId === tokenCategory.slice(0, 64)
        && input.token!.amount < 0n
        && nexcore.GroupToken.allowsSubgroup(input.token!.amount))[0];
      if (!parentGroup) {
        throw new Error('No authority supporting token subgroup creation found.');
      }

      selectedUtxos.push(parentGroup);
    }

    // genesis
    const genesisOutputs = outputs.filter((output) => output.token?.groupId === tokenCategory && output.token!.amount < 0n);
    if (genesisOutputs.length) {
      const authFlag = BigInt.asUintN(64, genesisOutputs[0].token!.amount);
      const hasNonce = nexcore.GroupToken.getNonce(authFlag) !== 0;
      const isAuth = nexcore.GroupToken.isAuthority(authFlag);
      if (!(hasNonce && isAuth)) {
        throw new Error('Invalid token genesis setup.');
      }
    }
  }

  const authorityUtxos = utxos.filter((utxo) => utxo.token?.groupId === tokenCategory && utxo.token!.amount < 0n);

  // positive imbalance, potential mint
  if (amountAvailable > 0n && amountAvailable < amountNeeded) {
    const allowsMint = authorityUtxos.some((auth) => nexcore.GroupToken.allowsMint(auth.token!.amount));
    if (!allowsMint) {
      throw new Error(`Insufficient funds for token ${tokenCategory}: available (${amountAvailable}) < needed (${amountNeeded}).`);
    }

    // add minting auth utxo to inputs
    selectedUtxos.push(authorityUtxos[0]);
  }

  // add matching authority inputs for authority outputs
  const authorityOutputs = outputs.filter((output) => output.token!.groupId === tokenCategory && output.token!.amount < 0n);
  authorityOutputs.forEach((output) => {
    const authInput = authorityUtxos.find((input) => deepEqual(input.token, output.token));
    if (!authInput) {
      throw new Error(`No matching authority utxo to transfer ${tokenCategory} with auth ${output.token?.amount}.`);
    }

    selectedUtxos.push(authInput);
  });

  // filter for unique utxos, auth inputs could have been added twice
  return selectedUtxos.filter((value, index, array) => array.findIndex(
    (other) => other.txid === value.txid && other.vout === value.vout,
  ) === index).sort(utxoTokenComparator);
};

const selectAllTokenUtxos = (utxos: Utxo[], outputs: Output[]): Utxo[] => {
  const tokenCategories = getTokenCategories(outputs);

  // needs another pass of unique filtering since subgroup categories can add their parent group auths
  return tokenCategories.flatMap(
    (tokenCategory) => selectTokenUtxos(utxos, outputs, calculateTotalTokenAmount(outputs, tokenCategory), tokenCategory),
  )
    .filter((value, index, array) => array.findIndex(
      (other) => other.txid === value.txid && other.vout === value.vout,
    ) === index);
};

const createFungibleTokenChangeOutputs = (utxos: Utxo[], outputs: Output[], address: string): Output[] => {
  const tokenCategories = getTokenCategories(utxos);

  const changeOutputs = tokenCategories.map((tokenCategory) => {
    const required = calculateTotalTokenAmount(outputs, tokenCategory);
    const available = calculateTotalTokenAmount(utxos, tokenCategory);
    const change = available - required;

    if (change <= 0n) return undefined;

    return { to: address, amount: BigInt(DUST_LIMIT), token: { groupId: tokenCategory, amount: change } };
  });

  return changeOutputs.filter((output) => output !== undefined) as Output[];
};

// Note: the below is a very simple implementation of a "decimal point" system for BigInt numbers
// It is safe to use for UTXO fee calculations due to its low numbers, but should not be used for other purposes
// Also note that multiplication and division between two "decimal" bigints is not supported

// High precision may not work with some 'number' inputs, so we set the default to 6 "decimal places"
const addPrecision = (amount: number | bigint, precision: number = 6): bigint => {
  if (typeof amount === 'number') {
    return BigInt(Math.ceil(amount * 10 ** precision));
  }

  return amount * BigInt(10 ** precision);
};

const removePrecisionFloor = (amount: bigint, precision: number = 6): bigint => (
  amount / (10n ** BigInt(precision))
);

const removePrecisionCeil = (amount: bigint, precision: number = 6): bigint => {
  const multiplier = 10n ** BigInt(precision);
  return (amount + multiplier - 1n) / multiplier;
};
