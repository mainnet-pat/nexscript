import {
  cashAddressToLockingBytecode,
  addressContentsToLockingBytecode,
  lockingBytecodeToCashAddress,
  binToHex,
  Transaction,
  generateSigningSerializationBCH,
  utf8ToBin,
  hexToBin,
  flattenBinArray,
  LockingBytecodeType,
} from '@bitauth/libauth';
import {
  AbiInput,
  ContractArtifact,
  ContractCreationParams,
  encodeInt,
  hash160,
  Op,
  Script,
  scriptToBytecode,
} from '@nexscript/utils';
import nexcore from 'nexcore-lib';
import {
  Utxo,
  Output,
  Network,
  Recipient,
  ContractDependencies,
} from './interfaces.js';
import {
  VERSION_SIZE,
  LOCKTIME_SIZE,
  DUST_LIMIT,
} from './constants.js';
import {
  OutputSatoshisTooSmallError,
  Reason,
  FailedTransactionError,
  FailedRequireError,
  FailedTimeCheckError,
  FailedSigCheckError,
} from './Errors.js';
import { Argument, encodeArgument } from './Argument.js';
import SignatureTemplate from './SignatureTemplate.js';

// ////////// PARAMETER VALIDATION ////////////////////////////////////////////
export function validateRecipient(recipient: Recipient): void {
  if (recipient.amount < DUST_LIMIT) {
    throw new OutputSatoshisTooSmallError(recipient.amount);
  }
}

// ////////// SIZE CALCULATIONS ///////////////////////////////////////////////
export function getInputSize(inputScript: Uint8Array): number {
  const scriptSize = inputScript.byteLength;
  const varIntSize = scriptSize > 252 ? 3 : 1;
  return 1 + 32 + varIntSize + scriptSize + 4 + 8;
}

export function getPreimageSize(script: Uint8Array): number {
  const scriptSize = script.byteLength;
  const varIntSize = scriptSize > 252 ? 3 : 1;
  return 4 + 32 + 32 + 36 + varIntSize + scriptSize + 8 + 4 + 32 + 4 + 4;
}

export function getTxSizeWithoutInputs(outputs: Output[]): number {
  // Transaction format:
  // Version (1 Bytes)
  // TxIn Count (1 ~ 9B)
  // For each TxIn:
  //   Type (1B)
  //   Outpoint (32B)
  //   Script Length (1 ~ 9B)
  //   ScriptSig(?)
  //   Sequence (4B)
  //   Sat amount (8B)
  // TxOut Count (1 ~ 9B)
  // For each TxOut:
  //   Type (1B)
  //   Value (8B)
  //   Script Length(1 ~ 9B)*
  //   Script (?)*
  // LockTime (4B)

  let size = VERSION_SIZE + LOCKTIME_SIZE;
  size += outputs.reduce((acc, output) => {
    if (typeof output.to === 'string') {
      const bufferLength = nexcore.Address.decodeNexaAddress(output.to).hashBuffer.length;
      const pushSize = bufferLength > 75 ? 2 : 1;
      return acc + 1 + 8 + pushSize + bufferLength;
    }

    // Size of an OP_RETURN output = byteLength + 8 (amount) + 2 (scriptSize)
    return acc + output.to.byteLength + 8 + 2;
  }, 0);
  // Add tx-out count (accounting for a potential change output)
  size += encodeInt(BigInt(outputs.length + 1)).byteLength;

  return size;
}

// ////////// BUILD OBJECTS ///////////////////////////////////////////////////
export function createInputScript(
  redeemScript: Script,
  encodedArgs: Uint8Array[],
  selector?: number,
  preimage?: Uint8Array,
): Uint8Array {
  // Create unlock script / redeemScriptSig (add potential preimage and selector)
  const unlockScript = encodedArgs.reverse();
  if (preimage !== undefined) unlockScript.push(preimage);
  if (selector !== undefined) unlockScript.push(encodeInt(BigInt(selector)));

  // Create input script and compile it to bytecode
  const inputScript = [...unlockScript, scriptToBytecode(redeemScript)];
  return scriptToBytecode(inputScript);
}

export function createOpReturnOutput(
  opReturnData: string[],
): Output {
  const script = [
    Op.OP_RETURN,
    ...opReturnData.map((output: string) => toBin(output)),
  ];

  return { to: encodeNullDataScript(script), amount: 0n };
}

function toBin(output: string): Uint8Array {
  const data = output.replace(/^0x/, '');
  const encode = data === output ? utf8ToBin : hexToBin;
  return encode(data);
}

export function createSighashPreimage(
  transaction: Transaction,
  input: Utxo,
  inputIndex: number,
  coveredBytecode: Uint8Array,
  hashtype: number,
): Uint8Array {
  const sourceOutputs = [];
  sourceOutputs[inputIndex] = { valueSatoshis: input.satoshis, lockingBytecode: Uint8Array.of() };
  const context = { inputIndex, sourceOutputs, transaction };
  const signingSerializationType = new Uint8Array([hashtype]);

  const sighashPreimage = generateSigningSerializationBCH(context, { coveredBytecode, signingSerializationType });

  return sighashPreimage;
}

export function buildError(reason: string, meepStr: string): FailedTransactionError {
  const require = [
    Reason.EVAL_FALSE, Reason.VERIFY, Reason.EQUALVERIFY, Reason.CHECKMULTISIGVERIFY,
    Reason.CHECKSIGVERIFY, Reason.CHECKDATASIGVERIFY, Reason.NUMEQUALVERIFY,
  ];
  const timeCheck = [Reason.NEGATIVE_LOCKTIME, Reason.UNSATISFIED_LOCKTIME];
  const sigCheck = [
    Reason.SIG_COUNT, Reason.PUBKEY_COUNT, Reason.SIG_HASHTYPE, Reason.SIG_DER,
    Reason.SIG_HIGH_S, Reason.SIG_NULLFAIL, Reason.SIG_BADLENGTH, Reason.SIG_NONSCHNORR,
  ];

  if (toRegExp(require).test(reason)) {
    return new FailedRequireError(reason, meepStr);
  }

  if (toRegExp(timeCheck).test(reason)) {
    return new FailedTimeCheckError(reason, meepStr);
  }

  if (toRegExp(sigCheck).test(reason)) {
    return new FailedSigCheckError(reason, meepStr);
  }

  return new FailedTransactionError(reason, meepStr);
}

function toRegExp(reasons: string[]): RegExp {
  return new RegExp(reasons.join('|').replace(/\(/g, '\\(').replace(/\)/g, '\\)'));
}

// ////////// MISC ////////////////////////////////////////////////////////////
export function meep(tx: any, utxos: Utxo[], script: Script): string {
  const scriptPubkey = binToHex(scriptToLockingBytecode(script));
  return `meep debug --tx=${tx} --idx=0 --amt=${utxos[0].satoshis} --pkscript=${scriptPubkey}`;
}

export function scriptTemplateToAddress(
  template: Script,
  constraints: Script,
  extraArgs: Script = [],
  network: string = 'mainnet',
): string {
  const templateHash = (template.length === 1 && template[0] === Op.OP_1 as any)
    ? Op.OP_1 : hash160(scriptToBytecode(template));
  const constraintHash = constraints.length ? hash160(scriptToBytecode(constraints)) : Op.OP_0;
  const addressObj = nexcore.Address.fromScriptTemplate(templateHash,
    constraintHash, binToHex(scriptToBytecode(extraArgs)), nexcore.Networks.get(network));
  const address = addressObj.toNexaAddress() as string;
  return address;
}

export function scriptTemplateToGroupLockingScript(
  groupId: string,
  amount: bigint,
  template: Script,
  constraints: Script,
  extraArgs: Script = [],
): any /* nexcore.Script */ {
  const templateHash = (template.length === 1 && template[0] === Op.OP_1 as any)
    ? Op.OP_1 : hash160(scriptToBytecode(template));
  const constraintHash = constraints.length ? hash160(scriptToBytecode(constraints)) : Op.OP_0;

  const scriptTemplate = nexcore.Script.empty()
    .add(Buffer.from(groupId, 'hex'))
    .add(nexcore.GroupToken.getAmountBuffer(amount))
    .add(templateHash)
    .add(constraintHash);

  extraArgs.forEach((arg) => scriptTemplate.add(arg));

  return scriptTemplate;
}

export function scriptTemplateToGroupAddress(
  groupId: string,
  amount: bigint,
  template: Script,
  constraints: Script,
  extraArgs: Script = [],
  network: string = 'mainnet',
): string {
  const script = scriptTemplateToGroupLockingScript(groupId, amount, template, constraints, extraArgs);
  const hashBuffer = nexcore.Script.empty().add(script.toBuffer()).toBuffer();
  const addressObject = new nexcore.Address(hashBuffer, nexcore.Networks.get(network),
    nexcore.Address.PayToScriptTemplate);
  const address = addressObject.toString();

  return address;
}

export function toGroupAddress(
  address: string,
  groupId: string,
  groupAmount: bigint,
  network: string = 'mainnet',
): string {
  const lockingScript = nexcore.Script.buildGroupedScriptTemplateOut(address, groupId, groupAmount);
  const addressScript = nexcore.Script.empty().add(lockingScript.toBuffer());

  return new nexcore.Address(addressScript.toBuffer(), nexcore.Networks.get(network),
    nexcore.Address.PayToScriptTemplate).toNexaAddress();
}

export function toNexaAddress(
  address: string,
  network: string = 'mainnet',
): string {
  const addressObject = nexcore.Address.decodeNexaAddress(address);
  const lockingScript = nexcore.Script.fromBuffer(nexcore.Script.fromBuffer(addressObject.hashBuffer).chunks[0].buf);

  if (lockingScript.chunks[0].opcodenum === Op.OP_0) {
    lockingScript.chunks = lockingScript.chunks.slice(1);
  } else {
    lockingScript.chunks = lockingScript.chunks.slice(2);
  }

  lockingScript.prepend(Op.OP_0);

  const addressScript = nexcore.Script.empty().add(lockingScript.toBuffer());

  return new nexcore.Address(addressScript.toBuffer(), nexcore.Networks.get(network),
    addressObject.type).toNexaAddress();
}

export function scriptToNexaAddress(
  lockingScript: any, // nexcore.Script,
  network: string = 'mainnet',
): string {
  const copy = new nexcore.Script(lockingScript);

  if (copy.chunks[0].opcodenum !== Op.OP_0) {
    copy.chunks = copy.chunks.slice(2);
    copy.prepend(Op.OP_0);
  }

  const addressScript = nexcore.Script.empty().add(copy.toBuffer());

  return new nexcore.Address(addressScript.toBuffer(), nexcore.Networks.get(network),
    nexcore.Address.PayToScriptTemplate).toNexaAddress();
}

export function scriptToAddress(script: Script, network: string): string {
  const lockingBytecode = scriptToLockingBytecode(script);
  const prefix = getNetworkPrefix(network);
  const address = lockingBytecodeToCashAddress(lockingBytecode, prefix as any) as string;
  return address;
}

export function scriptToLockingBytecode(script: Script): Uint8Array {
  const scriptHash = hash160(scriptToBytecode(script));
  const addressContents = { payload: scriptHash, type: LockingBytecodeType.p2sh20 };
  const lockingBytecode = addressContentsToLockingBytecode(addressContents);
  return lockingBytecode;
}

export function utxoComparator(a: Utxo, b: Utxo): number {
  if (a.satoshis > b.satoshis) return 1;
  if (a.satoshis < b.satoshis) return -1;
  return 0;
}

export function utxoTokenComparator(a: Utxo, b: Utxo): number {
  if (!a.token || !b.token) throw new Error('UTXO does not have token data');
  if (!a.token.groupId !== !b.token.groupId) throw new Error('UTXO token categories do not match');
  if (a.token.amount > b.token.amount) return 1;
  if (a.token.amount < b.token.amount) return -1;
  return 0;
}

/**
* Helper function to convert an address to a locking script
*
* @param address   Address to convert to locking script
*
* @returns a locking script corresponding to the passed address
*/
export function addressToLockScript(address: string): Uint8Array {
  const result = cashAddressToLockingBytecode(address);

  if (typeof result === 'string') throw new Error(result);

  return result.bytecode;
}

export function getNetworkPrefix(network: string): 'nexa' | 'nexatest' | 'nexareg' {
  switch (network) {
    case Network.MAINNET:
      return 'nexa';
    case Network.STAGING:
    case Network.TESTNET4:
    case Network.TESTNET:
    case Network.TESTNET3:
    case Network.CHIPNET:
      return 'nexatest';
    case Network.REGTEST:
      return 'nexareg';
    default:
      return 'nexa';
  }
}

// ////////////////////////////////////////////////////////////////////////////
// For encoding OP_RETURN data (doesn't require BIP62.3 / MINIMALDATA)
export function encodeNullDataScript(chunks: (number | Uint8Array)[]): Uint8Array {
  return flattenBinArray(
    chunks.map((chunk) => {
      if (typeof chunk === 'number') {
        return new Uint8Array([chunk]);
      }

      const pushdataOpcode = getPushDataOpcode(chunk);
      return new Uint8Array([...pushdataOpcode, ...chunk]);
    }),
  );
}

function getPushDataOpcode(data: Uint8Array): Uint8Array {
  const { byteLength } = data;

  if (byteLength === 0) return Uint8Array.from([0x4c, 0x00]);
  if (byteLength < 76) return Uint8Array.from([byteLength]);
  if (byteLength < 256) return Uint8Array.from([0x4c, byteLength]);
  throw Error('Pushdata too large');
}

export const getContractCreationParamsFromAddress = (address: string): ContractCreationParams => {
  const addressObject = nexcore.Address.decodeNexaAddress(address);
  const lockingBytecode = Uint8Array.from(nexcore.Script.fromBuffer(addressObject.hashBuffer).chunks[0].buf);
  const lockingScript = nexcore.Script.fromBuffer(nexcore.Script.fromBuffer(addressObject.hashBuffer).chunks[0].buf);

  let constraintIndex = 2;
  if (lockingScript.chunks[0].opcodenum !== Op.OP_0) {
    constraintIndex = 3;
  }

  const constraintHash = Uint8Array.from(lockingScript.chunks[constraintIndex].buf ?? [0x00]);
  lockingScript.chunks = lockingScript.chunks.slice(constraintIndex + 1);

  const visibleArgs = Uint8Array.from(lockingScript.toBuffer());

  return {
    constraintHash,
    visibleArgs,
    lockingBytecode,
  };
};

export const encodeArguments = (constructorArgs: Argument[], constructorInputs: AbiInput[], contractName: string): {
  encodedArgs: Uint8Array[],
  visibleArgs: Uint8Array[],
} => {
  if (constructorInputs.length !== constructorArgs.length) {
    throw new Error(`Incorrect number of arguments passed to ${contractName} constructor`);
  }

  // Encode arguments (this also performs type checking)
  const encodedArgs = constructorArgs
    .map((arg, i) => encodeArgument(arg, constructorInputs[i].type) as Uint8Array)
    .filter((arg, i) => !constructorInputs[i].visible);

  const visibleArgs = constructorArgs
    .map((arg, i) => encodeArgument(arg, constructorInputs[i].type) as Uint8Array)
    .filter((arg, i) => constructorInputs[i].visible);

  // Check there's no signature templates in the constructor
  if (encodedArgs.some((arg) => arg instanceof SignatureTemplate)) {
    throw new Error('Cannot use signatures in constructor');
  }

  return {
    encodedArgs,
    visibleArgs,
  };
};

export const replaceDependencyArgs = (artifact: ContractArtifact, dependencyArgs?: ContractDependencies)
:{ [contractName: string]: ContractCreationParams } => {
  const result: { [contractName: string]: ContractCreationParams } = {};

  // convert dependencyArgs to ContractCreationParams
  Object.entries(dependencyArgs ?? {}).forEach(([key, value]) => {
    if ('constructorArgs' in value) {
      const { encodedArgs, visibleArgs } = encodeArguments(
        value.constructorArgs,
        value.constructorInputs,
        key,
      );

      const constraintHash = encodedArgs.length ? hash160(scriptToBytecode(encodedArgs)) : Uint8Array.from([0x00]);

      result[key] = {
        constraintHash,
        visibleArgs: scriptToBytecode(visibleArgs),
        lockingBytecode: Uint8Array.from([]),
      };
    } else {
      result[key] = value;
    }
  });

  artifact.dependencies?.forEach((dependencyContractName) => {
    if (!result[dependencyContractName]) {
      throw new Error(`No contract creation parameters provided for dependency contract '${dependencyContractName}'`);
    }

    // replace Contract.constraintHash
    artifact.bytecode = artifact.bytecode.replaceAll(
      binToHex(utf8ToBin(`${dependencyContractName}.constraintHash`)),
      binToHex(result[dependencyContractName].constraintHash),
    );

    // replace Contract.visibleParams
    artifact.bytecode = artifact.bytecode.replaceAll(
      binToHex(utf8ToBin(`${dependencyContractName}.visibleParams`)),
      binToHex(result[dependencyContractName].visibleArgs),
    );

    // replace Contract.lockingBytecode
    const lockingByteCodePattern = binToHex(utf8ToBin(`${dependencyContractName}.lockingBytecode`));
    if (artifact.bytecode.includes(lockingByteCodePattern)) {
      if (!result[dependencyContractName].lockingBytecode.length) {
        throw new Error(`Locking bytecode for contract ${dependencyContractName} was not defined. Use \`getContractCreationParamsFromAddress\` util`);
      }
      artifact.bytecode = artifact.bytecode.replaceAll(
        lockingByteCodePattern,
        binToHex(result[dependencyContractName].lockingBytecode),
      );
    }
  });

  return result;
};
