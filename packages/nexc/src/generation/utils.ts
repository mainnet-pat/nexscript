import {
  asmToScript,
  BytesType,
  encodeInt,
  Op,
  PrimitiveType,
  Script,
  Type,
} from '@nexscript/utils';
import { UnaryOperator, BinaryOperator, NullaryOperator } from '../ast/Operator.js';
import { GlobalFunction, TimeOp } from '../ast/Globals.js';

export function compileTimeOp(op: TimeOp): Script {
  const mapping = {
    [TimeOp.CHECK_LOCKTIME]: [Op.OP_CHECKLOCKTIMEVERIFY, Op.OP_DROP],
    [TimeOp.CHECK_SEQUENCE]: [Op.OP_CHECKSEQUENCEVERIFY, Op.OP_DROP],
  };

  return mapping[op];
}

export function compileCast(from: Type, to: Type): Script {
  if (from === PrimitiveType.INT && to instanceof BytesType && to.bound !== undefined) {
    return [encodeInt(BigInt(to.bound)), Op.OP_NUM2BIN];
  }

  if (from !== PrimitiveType.INT && to === PrimitiveType.INT) {
    return [Op.OP_BIN2NUM];
  }

  return [];
}

export function compileGlobalFunction(fn: GlobalFunction): Script {
  const mapping = {
    [GlobalFunction.ABS]: [Op.OP_ABS],
    [GlobalFunction.CHECKDATASIG]: [Op.OP_CHECKDATASIG],
    [GlobalFunction.CHECKMULTISIG]: [Op.OP_CHECKMULTISIG],
    [GlobalFunction.CHECKSIG]: [Op.OP_CHECKSIG],
    [GlobalFunction.MAX]: [Op.OP_MAX],
    [GlobalFunction.MIN]: [Op.OP_MIN],
    [GlobalFunction.RIPEMD160]: [Op.OP_RIPEMD160],
    [GlobalFunction.SHA1]: [Op.OP_SHA1],
    [GlobalFunction.SHA256]: [Op.OP_SHA256],
    [GlobalFunction.HASH160]: [Op.OP_HASH160],
    [GlobalFunction.HASH256]: [Op.OP_HASH256],
    [GlobalFunction.WITHIN]: [Op.OP_WITHIN],

    // actual handling is located in `visitTxStateFunction`
    [GlobalFunction.GROUP_AMOUNT_IN]: [Op.OP_PUSH_TX_STATE],
    [GlobalFunction.GROUP_AMOUNT_OUT]: [Op.OP_PUSH_TX_STATE],
    [GlobalFunction.GROUP_COUNT_IN]: [Op.OP_PUSH_TX_STATE],
    [GlobalFunction.GROUP_COUNT_OUT]: [Op.OP_PUSH_TX_STATE],
    [GlobalFunction.GROUP_NTH_INPUT]: [Op.OP_PUSH_TX_STATE],
    [GlobalFunction.GROUP_NTH_OUTPUT]: [Op.OP_PUSH_TX_STATE],
  };

  return mapping[fn];
}

export function compileBinaryOp(op: BinaryOperator, numeric: boolean = false): Script {
  const mapping: { [key in BinaryOperator]: Script } = {
    [BinaryOperator.MUL]: [Op.OP_MUL],
    [BinaryOperator.DIV]: [Op.OP_DIV],
    [BinaryOperator.MOD]: [Op.OP_MOD],
    [BinaryOperator.PLUS]: [Op.OP_CAT],
    [BinaryOperator.MINUS]: [Op.OP_SUB],
    [BinaryOperator.LT]: [Op.OP_LESSTHAN],
    [BinaryOperator.LE]: [Op.OP_LESSTHANOREQUAL],
    [BinaryOperator.GT]: [Op.OP_GREATERTHAN],
    [BinaryOperator.GE]: [Op.OP_GREATERTHANOREQUAL],
    [BinaryOperator.EQ]: [Op.OP_EQUAL],
    [BinaryOperator.NE]: [Op.OP_EQUAL, Op.OP_NOT],
    [BinaryOperator.AND]: [Op.OP_BOOLAND],
    [BinaryOperator.OR]: [Op.OP_BOOLOR],
    [BinaryOperator.BIT_AND]: [Op.OP_AND],
    [BinaryOperator.BIT_OR]: [Op.OP_OR],
    [BinaryOperator.BIT_XOR]: [Op.OP_XOR],
    [BinaryOperator.SPLIT]: [Op.OP_SPLIT],
  };

  if (numeric) {
    mapping[BinaryOperator.PLUS] = [Op.OP_ADD];
    mapping[BinaryOperator.EQ] = [Op.OP_NUMEQUAL];
    mapping[BinaryOperator.NE] = [Op.OP_NUMNOTEQUAL];
  }

  return mapping[op];
}

export function compileUnaryOp(op: UnaryOperator): Script {
  const mapping = {
    [UnaryOperator.NOT]: [Op.OP_NOT],
    [UnaryOperator.NEGATE]: [Op.OP_NEGATE],
    [UnaryOperator.SIZE]: [Op.OP_SIZE, Op.OP_NIP],
    [UnaryOperator.REVERSE]: [Op.OP_REVERSEBYTES],
    [UnaryOperator.INPUT_VALUE]: [Op.OP_UTXOVALUE],
    [UnaryOperator.INPUT_LOCKING_BYTECODE]: [Op.OP_UTXOBYTECODE],
    [UnaryOperator.INPUT_OUTPOINT_HASH]: [Op.OP_OUTPOINTTXHASH],
    [UnaryOperator.INPUT_OUTPOINT_INDEX]: [Op.OP_OUTPOINTINDEX],
    [UnaryOperator.INPUT_UNLOCKING_BYTECODE]: [Op.OP_INPUTBYTECODE],
    [UnaryOperator.INPUT_SEQUENCE_NUMBER]: [Op.OP_INPUTSEQUENCENUMBER],
    [UnaryOperator.OUTPUT_VALUE]: [Op.OP_OUTPUTVALUE],
    [UnaryOperator.OUTPUT_LOCKING_BYTECODE]: [Op.OP_OUTPUTBYTECODE],

    // the operations below are emulated until next hardfork where they
    // are expected to be implemented as OP_PUSH_TX_STATE subroutines

    // eslint-disable-next-line
    // https://ide.bitauth.com/import-template/eJzFWG1v2zYQ_iuEsE-tEh8pUpSCNkCbeFiBYPaaBNvQFAbfFGu1JcGSmwFF_vsoKWmTLLHDxrT1QeDLkXzu7uHxyG_BL7WamrkIDoJp01T1wWCQa7Mv80Ysm-m-KueDtmCKJleiyctirzHzaiYas_cV9vux-__UZRGEgTa1WuRVK2Wnsw2FmBtbOreDm5nRtqWdp8lNHRx8uw6DXryttEXRqOmkErqt3ow87VvRWLSDe3HbPBig0XgC7e_87K_R-7_Phkej4-FFcVHYrsErdLkol1WIxLxcFg06xHGIKlJ9aUKkyqJuFiIvbFksLmv0aoDewL8E0hd-QEwCEcOYkmd-QDIKGAhLgBw-jvzNNpAzvGHUbxFswd7wAtRuPCnKXVPFkR6bBuzGEEdWbNy6TsR4FhksRO9xA8AxHvgE1Tr8WYDuOtWrleARQM6O87WRAdz95WOPPoljhZt8bL8Oxxr_1P4PY0xTJmgSM0ziGDPAsVZSxgmPJKaRkjGnWco5EcTHoe1VwTbE7US5Gyb59R5sQTkncvoJHD60dOPk5oPQFnV6goqbD2iedFrLQP9px_M1c0lPfGN3YZlLFuPd5g5Mckt2HkG-65C1LifaAmQ3njhzY5eB5ibDugMYvUb1Ut7Hjh-5cD2ATUEJlUU6ZQmOsUgJUyQRKQcdKQraGIVFlBmIeRpnBksquAWURZwrLS1kANsgmSBExSxlmnJlhRnR3FgNqOJxqoALxQknGFjKE9BCGcUzSkVKY5srEsvy7s5mu2VERSYNaCWM1JJjjDOmBLNgBJWZJgyIulW90w1d5c201yiv0dyIIi8uZ6auQzQtr8xXs0B5hspmagtXZjbb-1KUVwW6fcWyw2cztKwNshLz0P7tLKrUphJ22npaLmcaSdtr6sboW4tbG95YevUzR6-XSgmPdGYokzhJIaGZVopERgKJKY0kZ7E1EAf2Iz6tXOC-B93WOHxouXuk3qUJn468_ZVilXVW7sw7N5L_D1-RyLod_C908uosxg-UJ4GsMedPRdU1a_z0Aqw_HEfjCW7fg0_HJx_OusKf78aoaz8-bwvdxIdtz_CP83cnXc-HXzt0t9C6NnL8cTTunpe76vDkdNgJ_ZD4vkQr2U1M-okfdLSjfz9u17goguv2uXtZVeXC7oHg4FPw_ui3ib2TkAmwIOxqp-PhUfA5DOx-q7sndLj-DzH4UHI=
    [UnaryOperator.INPUT_TOKEN_GROUP_ID]: asmToScript('OP_UTXOBYTECODE OP_1 OP_SPLIT OP_SWAP OP_DUP 00 OP_EQUAL OP_IF OP_2DROP OP_0 OP_ELSE OP_SPLIT OP_DROP 20 OP_SPLIT OP_DROP OP_ENDIF'),
    [UnaryOperator.OUTPUT_TOKEN_GROUP_ID]: asmToScript('OP_OUTPUTBYTECODE OP_1 OP_SPLIT OP_SWAP OP_DUP 00 OP_EQUAL OP_IF OP_2DROP OP_0 OP_ELSE OP_SPLIT OP_DROP 20 OP_SPLIT OP_DROP OP_ENDIF'),

    // eslint-disable-next-line
    // https://ide.bitauth.com/import-template/eJzFWG1v2zYQ_iuEsE-dEh8pUpSCzkCbeFiAYHaXBFu7DAbfFGu1JcGSmwFF_nspyc5Ll9hhY9n6IPDlSD539_B45Ffvp1JNzEx4R96kqoryqNdLtTmUaSUW1eRQ5bNeXTBZlSpRpXl2UJlZMRWVOfgCh-3Yw3_LPPN8T5tSzdOilrLT2YZMzIwtXdrB1dRo21LPU6Wm9I6-3vpeK15X6qKo1GRcCF1XlyPP21Y0EvXgVtw293poOBpD_bu8-Gv4_uPF4Hh4MrjKrjLb1XuDruf5ovCRmOWLrEJ9HPqoIMXnykcqz8pqLtLMlsX8ukRveugt_EcgfuUHxEQQMIwpeeEHJKGAgbAISP9p5G93gZzhLaP-BcEO7A2vQO3GkyzfN1Uc6bFtwG4McWTF1q3rRIwXkcFC7DxuADjGgy5B1Q5_EaCHTu3USvAEIGfHdbWRAdz91cUefRbHGjd1sf0aHBv8U3Z_GGMaM0GjkGEShpgBDrWSMox4IDENlAw5TWLOiSBdHNqdKliHuL0ot2RSt96DHSjnRM5uAkcXWrpxcvtBaIc6PUPF7Qe0jnTayMDu046Xa-aSnnSN3YVlLllM5zZ3YJJbsvME8n2HrE050Q4gu_HEmRv7DDTLDOsBYPQzKhfyMXb8xIXrO9gUlFBJoGMW4RCLmDBFIhFz0IGioI1RWASJgZDHYWKwpIJbQEnAudLSQgawDZIJQlTIYqYpV1aYEc2N1YAqHsYKuFCccIKBxTwCLZRRPKFUxDS0uSKxLG_ubLZbBlQk0oBWwkgtOcY4YUowC0ZQmWjCgKiV6o1u6CatJq1GaYlmRmRpdj01ZemjSX5jvpg5ShOUVxNbuDHT6cHnLL_J0OoVyw6fTtGiNMhKzHz7t7OoXJtC2GnLSb6YaiRtrykro1cWtzZcWnr9M0erl4oJD3RiKJM4iiGiiVaKBEYCCSkNJGehNRAHdh-f1i7w2INua_S_t9wjUu_ThM9H3vZKsc46a3fmgxvJ_4evSWTdDv5XOnl9FtMNlGeBbDDnD0XVDWv88AKsPRyHozGu34PPR2enF03hz3cj1LSfXNaFZuJ-3TP4cPnurOk5_fUqQ_azCFfw2gbbR07-GI6aZ-ZGdHB2PrgTfizZLHlXq4fdd51-Wo5qToj-XccSw7K2wvFgivaB-17-95Na6L50lXm39Qv6oijyud1W3tHf3vvj38b2mkPGwDy_qZ2PBsfeP75nt3DZvMrD7Tf8lmJS
    [UnaryOperator.INPUT_TOKEN_SUBGROUP_ID]: asmToScript('OP_UTXOBYTECODE OP_1 OP_SPLIT OP_SWAP OP_DUP 00 OP_EQUAL OP_IF OP_2DROP OP_0 OP_ELSE OP_SPLIT OP_DROP OP_SIZE 20 OP_EQUAL OP_IF OP_DROP OP_0 OP_ENDIF OP_ENDIF '),
    [UnaryOperator.OUTPUT_TOKEN_SUBGROUP_ID]: asmToScript('OP_OUTPUTBYTECODE OP_1 OP_SPLIT OP_SWAP OP_DUP 00 OP_EQUAL OP_IF OP_2DROP OP_0 OP_ELSE OP_SPLIT OP_DROP OP_SIZE 20 OP_EQUAL OP_IF OP_DROP OP_0 OP_ENDIF OP_ENDIF '),

    // eslint-disable-next-line
    // https://ide.bitauth.com/import-template/eJzFWG1v2zYQ_iuEsE-tEh8pUpSCNkCbeFiAoPaWBNvQFAbfFGu1JcGimwFF_vsoKWmTLrHDxrL9QebLkXzu7uHxyK_BL7WamrkIDoKptVV9MBjk2uzL3Iqlne6rcj5oCqawuRI2L4s9a-bVTFiz9wX2u7H7_9RlEYSBNrVa5FUj5aZzDYWYG1e6cIPtzGjX0sxjc1MHB19vwqATbypNUVg1nVRCN9XbkWddKxqLZnAn7poHAzQaT6D5XJz_NXr_9_nwaHQ8vCwuC9c1eIWuFuWyCpGYl8vCokMch6gi1WcbIlUWtV2IvHBlsbiq0asBegP_Ekhf-ANiEogYxpQ88wcko4CBsATI4ePI32wDOcMbRv0WwRbsDS9A7ceTotw1VTzpsWnAfgzxZMXGretFjGeRwUHsPW4AeMaDPkE1Dn8WoPtO7dVK8Aggb8f1tZEB_P3Vxx59EscKN_Wx_Voca_xT938YY5oyQZOYYRLHmAGOtZIyTngkMY2UjDnNUs6JIH0c2r0q2IS4nSh3y6R-vQdbUM6LnP0Ejj609OPk5oPQFnV6goqbD2g96bSWgf2nHc_XzCc96Ru7D8t8spjebe7BJL9k5xHkuw5Z63KiLUD244k3N3YZaG4zrHuA0WtUL-VD7PiRC9cPsCkoobJIpyzBMRYpYYokIuWgI0VBG6OwiDIDMU_jzGBJBXeAsohzpaWDDOAaJBOEqJilTFOunDAjmhunAVU8ThVwoTjhBANLeQJaKKN4RqlIaexyReJY3t7ZXLeMqMikAa2EkVpyjHHGlGAOjKAy04QBUXeqt7qh69xOO43yGs2NKPLiambqOkTT8tp8MQuUZ6i0U1e4NrPZ3ueivC7Q3SuWGz6boWVtkJOYh-7rZlGlNpVw09bTcjnTSLpeU1uj7yzubHhr6dXPHJ1eKiU80pmhTOIkhYRmWikSGQkkpjSSnMXOQBzY9_i0coGHHvRb4_BHyz0g9S5N-HTk7a4Uq6yzcmfeu5H8f_iKRNbv4H-hk1dnMf1AeRLIGnP-VFRds8ZPL8C6w3E0nuDmPfhsfHpy3hb-fDdum48v3H877WHTPvz94t1p23Hya4vtDljbRo7_GI3bx-W2Ojw9G7ZC3yW-LdBIthOTbuJvHR9OuoWHH46bJS6L4KZ5615WVblwGyA4-Bi8P_pt4i4kZAIsCNva2Xh4FHwKA7fZ6vb9HG7-A4a3T-Q=
    [UnaryOperator.INPUT_SUBGROUP_DATA]: asmToScript('OP_UTXOBYTECODE OP_1 OP_SPLIT OP_SWAP OP_DUP 00 OP_EQUAL OP_IF OP_2DROP OP_0 OP_ELSE OP_SPLIT OP_DROP 20 OP_SPLIT OP_NIP OP_ENDIF'),
    [UnaryOperator.OUTPUT_SUBGROUP_DATA]: asmToScript('OP_OUTPUTBYTECODE OP_1 OP_SPLIT OP_SWAP OP_DUP 00 OP_EQUAL OP_IF OP_2DROP OP_0 OP_ELSE OP_SPLIT OP_DROP 20 OP_SPLIT OP_NIP OP_ENDIF'),

    // eslint-disable-next-line
    // https://ide.bitauth.com/import-template/eJzFWG1P3DgQ_itWdJ_awI4dO07QHVKBPR0SZfcKq_Z0VCu_hc11N4k22VKp4r_fJFnKS-lCSgP5kIwnY88zj8djJ1-930ozcwvl7XizqirKncEgtW5bp5VaVbNtky8GteCyKjWqSvNsq3KLYq4qt_UZttu-2_-Veeb5nnWlWaZFbYXDoSJTC4fSBDtXc2dRU49Tpa70dr5e-l5rXjdqUVVmNi2UrZvrnietloxV3bk1R_VgQEbjKdS3yemH0d4_p8P90cHwLDvL8NXgFTlf5qvCJ2qRr7KK7BLwScGKT5VPTJ6V1VKlGcpqeV6SVwPyO3xhED_xAuYiCASlnD3yApZwoMBEBGz3fuR_PAtyeALqboxn-UuT3pHoXw64E9eP4hch9p7UAB2TtVdQcA-gzjz1lYoA3enpI8saHBtZKfsvh5THQvEoFJSFIRVAQ2u0DiMZaMoDo0PJk1hKplgfZbPfAOEZgus0f_1kdB9Rdpu2X786eorpwdnqv1Q_PrIuJb137B1mpFvlvwf5Sy-ThzaI54Dcie-7HJPXpFzp29jpPaenO7A5GGWSwMYioiFVMROGRSqWYAPDwTpnqAoSB6GMw8RRzZVEQEkgpbEaIQOgQgvFmAlFLCyXBo0Fs9JhBNzIMDYglZFMMgoilhFYZZyRCecq5iHuiAzTpjmA4WsdcJVoB9Yop62WlNJEGCUQjOI6sUwAM1ehN7GRi7SatRGlJVk4laXZ-dyVpU9m-YX77JYkTUiOH0tLcuHm861PWX6RkavvJew-n5NV6QhaLHy84ygmt65QOGw5y1dzSzS-dWXl7BXjyOGa6c2fAW1cJmYysInjQtMohogn1hgWOA0s5DzQUoRIkARxveA3Org9g9187N5l7lZSvySFPy5l7cFpEzsbV-Z3566b3TecRrrtSE-c5M3baz9QfgjkATp_qqo-4OOnHYh2txmNp7T-83AyPjo8bYT3b8b182AybgfdrVvDvydvjhrrwz9J82QH70aNITTN4dHJsIHcgG1U34Y8Phyvk6itr63Xa5tWQr9rQOj6LPvON8Gr8d-K6-ZNGGtVC-WGzdrJDU3d6ZZi7_CYHU_eXo9xfFA7upa8y_oPz6oo8iUuRm_nX29v_68pnnDZFITnN62T8XDf--h7uPDL5q8RXP4PeQULfg==
    [UnaryOperator.INPUT_TOKEN_AMOUNT]: asmToScript('OP_UTXOBYTECODE  OP_1 OP_SPLIT OP_SWAP OP_DUP 00 OP_EQUAL OP_IF OP_2DROP OP_0 OP_ELSE OP_SPLIT OP_NIP OP_1 OP_SPLIT OP_SWAP OP_DUP 00 OP_EQUAL OP_IF OP_2DROP OP_0 OP_ELSE OP_SPLIT OP_DROP OP_BIN2NUM OP_ENDIF OP_ENDIF'),
    [UnaryOperator.OUTPUT_TOKEN_AMOUNT]: asmToScript('OP_OUTPUTBYTECODE  OP_1 OP_SPLIT OP_SWAP OP_DUP 00 OP_EQUAL OP_IF OP_2DROP OP_0 OP_ELSE OP_SPLIT OP_NIP OP_1 OP_SPLIT OP_SWAP OP_DUP 00 OP_EQUAL OP_IF OP_2DROP OP_0 OP_ELSE OP_SPLIT OP_DROP OP_BIN2NUM OP_ENDIF OP_ENDIF'),

    // eslint-disable-next-line
    // https://ide.bitauth.com/import-template/eJzFWG1v2zYQ_iuEsE-dEpMUKUrBZqBNXNRAEGuLg25YBoNvirXakmDJTYEi_30UZccvdRWrqRJ9kI6nI--5h8cjpa_OL4Wc6jl3zpxpWebFWa-XKH0qkpIvy-mpzOa9StBpmUheJll6Uup5PuOlPvkMT-u-p_8VWeq4jtKFXCR5ZWWGM4qUz7WRbkzncqaV0VTjlIkunLOvD65Tm1eNSuSlnE5yrqrmqud1rQURrzrX5kbd64FRNIHV7Wb81-jd3-PB-ehicJvepuZV7w24W2TL3AV8ni3TEvQBdEGO80-lC2SWFuWCJ6mR-eKuAG964Df4BcPwmRfEOoAeRYjgIy-IYwIRxDSAuH8Y-e8vghw-A3U7xtPstUlvSfRPB9yK66P4NRA7T2oIWyZrp6DgAUCteeoqFSFsT08XWWZxNLJSdF8OEQkpJ4FPEfZ9RCHylRTCD5gnEPGk8BmJQ8Ywx12UzW4DhC8QXKv56yaju4iy3bT9_NXRUUxPzlb3pfr4yNqU9M6xt5iRdpX_APLXXiZPbRAvAbkV3_scg19BsRS72NGB09MebAIll7GnQhogH_EQU4kDHjKoPEmg0loi7sUa-iz0Y40E4cwAij3GpBIGMoRGISjHWPo0pIowaYwpVkybCIhkfigh45JhhhGkIQug4lJLFhPCQ-KbHRGbtLEHMPNaeITHQkMluRZKMIRQTCWnBgwnIlaYQizXodvYwH1STuuIkgLMNU-T9G6mi8IF0-xef9YLkMQgMx9LC3CvZ7OTT2l2n4L195LpPpuBZaGBsZi75m5GkZnSOTfDFtNsOVNAmLe6KLVaM244XDHd_BlQxyVDzDwVa0IFCkIYkFhJiT0tIPYJ8QSjviGIQbpZ8I0OdmewnY_-PnM7Sf2aFH6_lNUHpyZ2GlfmN-eu7e4Np5F2O9IzJ7l5e-0GyneBPEHnD1XVJ3z8sANa7zajaIKqPw_X0eVwbIWPb6PqeXET1YP2q9bgj5u3l9Z6-B7Y58Wfo8gKg8vrgQVrYVrV42BXw2iVPnVlrf1tbGrJeFxBMU5v0z2vdpzRePj-NgXm2kCjqF89_Nr243D8YXj1aLPVY6VZOdzSWHRbbet9JQ-uLqr-O9Im5Fplwnpcy1NeTJvZtIE1Yt4DXLtb47EsHxfOBqHFuFWmjkR5YNI35O_O-7do1ki2UTgP1f-xZZ5nC1PKnLN_nHfnHybm-wBPIHVc27qOBufOv65jymZh_7nBh_8Bq7to6A==// https://ide.bitauth.com/import-template/eJzFWW1v4jgQ_itWdJ_20mI7dkyqPaTdltUiVYW7Uu2drifk2E6TW0giErYnrfrfz3mhQJcGvCWQD2TsjD3PzDye2OG79UsmQjXj1oUV5nmaXXQ6kVTnfpTzRR6ei2TWKQQV55HgeZTEZ7mapVOeq7Nv8Lwae_5vlsSWbUmViXmUFlp6Ot0R85nS0p0enE-V1D3FPHmkMuvi-5NtVepFoxB5LsJJymXRrEfeVr1gxIvBlbru7nTAcDSBxc_d-M_hx7_G_cvhVf8-vo_1o8478DBPFqkN-CxZxDnoIdcGKU6_5jYQSZzlcx7FWubzhwy864D38D8MvTdeEKsudChCBO95QRwQiCCmXYh725G_PwZyig6M-jcAjxBv-AbUZjyJk1NTxZAehwZsxhBDVhw8ukbE2IsMGmLrdQNCw3rQJqgi4XsBWk9qq1GCWwAZJ66thQyheb7aWKOv4mhIUxvLr8SxIz9Z-y9jRDzKSdelCLsuohC5Uvi-22WOj4gjfJeRwGMMc9zGS7tVB4sSdxLnaia1mz14BOeMyNlO4WjDSzNOHr4IHdGnV6h4-ILWkk87Gdj-tmN_z0y2J21jN2GZyS6m9ZgbMMlss7MF-alL1q490REgm_HEmBunLDT1DmsNMPgVZAt_EzvacuB6AZtAwUXgSI92kYu4h6nAXe4xKB1BoFRKIO4ECrrMcwOFfMKZBhQ4jAnpa8gQ6g6fcoyFSz0qCRNamWLJlPaACOZ6AjIuGGYYQeqxLpRcKMECQrhHXL1XxJrl5ZlNP_YdwgNfQSm48qXPEEIBFZxqMJz4gcQUYrF0vfQNPEZ5WHkUZWCmeBzFD1OVZTYIk0f1Tc1BFIAkD7XwqKbTs69x8hiD5VcsPXw6BYtMAa0xs_WvnkUkUqVcT5uFyWIqga-fqixXchlxHcM60s2fOSq_hIeZIwNFqI-6HuySQAqBHeVD7BLi-Iy6OkAM0lV9ajSwmUEzG72Xkdsg9SlD-HrlrY4UTdFpXJlrJ5IfhzdsZM1e_G9McvMuph0orwLZEc6fqqo7bPy0AVq9HIejCSq-B9-OrgfjUvjyYVTcr-5G1aS9otX__e7Ddak9-ATK-9Ufw1Ep9K9v-yXYEmbZ9TzZzWBU06eqrJW9lU4laYs1FG30Pn5htZxnOB58uo-BvlbQKOoVN7fS_TIYfx7cPOusjah7aoNrPSW6tXZpvZb7N1fF-A1p5XLVpd16Xsshz8LmaJaONWJ-Abgyt8RTRnk_d1YIS4xrZWpPlFuSvgr-Zt5_RLNEso7Ceir-tVikaTLXpcy6-Nv6ePl5oo-WeAKpZZet21H_0vrHtnTZzMp_QuDT_4B80W0=
    [UnaryOperator.INPUT_VISIBLE_PARAMETERS]: asmToScript('OP_UTXOBYTECODE OP_1 OP_SPLIT OP_SWAP OP_DUP 00 OP_EQUAL OP_IF OP_DROP OP_ELSE OP_SPLIT OP_NIP OP_1 OP_SPLIT OP_SWAP OP_DUP 00 OP_EQUAL OP_NOTIF OP_DUP 51 60 OP_WITHIN OP_NOTIF OP_SPLIT OP_NIP OP_DUP OP_ENDIF OP_ENDIF OP_DROP OP_ENDIF OP_1 OP_SPLIT OP_SWAP OP_DUP 51 60 OP_WITHIN OP_IF OP_DROP OP_ELSE OP_SPLIT OP_NIP OP_ENDIF OP_1 OP_SPLIT OP_SWAP OP_DUP 00 OP_EQUAL OP_IF OP_DROP OP_ELSE OP_SPLIT OP_NIP OP_ENDIF'),
    [UnaryOperator.OUTPUT_VISIBLE_PARAMETERS]: asmToScript('OP_OUTPUTBYTECODE OP_1 OP_SPLIT OP_SWAP OP_DUP 00 OP_EQUAL OP_IF OP_DROP OP_ELSE OP_SPLIT OP_NIP OP_1 OP_SPLIT OP_SWAP OP_DUP 00 OP_EQUAL OP_NOTIF OP_DUP 51 60 OP_WITHIN OP_NOTIF OP_SPLIT OP_NIP OP_DUP OP_ENDIF OP_ENDIF OP_DROP OP_ENDIF OP_1 OP_SPLIT OP_SWAP OP_DUP 51 60 OP_WITHIN OP_IF OP_DROP OP_ELSE OP_SPLIT OP_NIP OP_ENDIF OP_1 OP_SPLIT OP_SWAP OP_DUP 00 OP_EQUAL OP_IF OP_DROP OP_ELSE OP_SPLIT OP_NIP OP_ENDIF'),
  };

  return mapping[op];
}

export function compileNullaryOp(op: NullaryOperator): Script {
  const mapping = {
    [NullaryOperator.INPUT_INDEX]: [Op.OP_INPUTINDEX],
    [NullaryOperator.BYTECODE]: [Op.OP_ACTIVEBYTECODE],
    [NullaryOperator.INPUT_COUNT]: [Op.OP_TXINPUTCOUNT],
    [NullaryOperator.OUTPUT_COUNT]: [Op.OP_TXOUTPUTCOUNT],
    [NullaryOperator.VERSION]: [Op.OP_TXVERSION],
    [NullaryOperator.LOCKTIME]: [Op.OP_TXLOCKTIME],
    [NullaryOperator.ID]: [Op.OP_2, Op.OP_PUSH_TX_STATE],
    [NullaryOperator.IDEM]: [Op.OP_3, Op.OP_PUSH_TX_STATE],
    [NullaryOperator.AMOUNT_IN]: [Op.OP_5, Op.OP_PUSH_TX_STATE],
    [NullaryOperator.AMOUNT_OUT]: [Op.OP_6, Op.OP_PUSH_TX_STATE],
  };

  return mapping[op];
}
