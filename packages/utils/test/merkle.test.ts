import { binsAreEqual, hash160, hexToBin, utf8ToBin } from "@bitauth/libauth";
import { MerkleTree } from "merkletreejs";
import { GetNewCompactProof, ToCompactProof, ToFullProof, ulog2 } from "../src/mekle.js";

const hash = (value: string | Buffer): Buffer => Buffer.from(hash160(typeof value === 'string' ? utf8ToBin(value as string) : value as any));
const leaves = [
  "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
  "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
  "aa", "bb", "cc", "dd", "ee", "ff", "gg", "hh", "ii", "jj", "kk", "ll", "mm", "nn", "oo", "pp", "qq", "rr", "ss", "tt", "uu", "vv", "ww", "xx", "yy", "zz",
  "AA", "BB", "CC", "DD", "EE", "FF", "GG", "HH", "II", "JJ", "KK", "LL", "MM", "NN", "OO", "PP", "QQ", "RR", "SS", "TT", "UU", "VV", "WW", "XX", "YY", "ZZ",
  "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
  "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
];
const hashes = leaves.map(val => hash(val));

it('merkle tools', () => {
  let prevProof: Uint8Array[];
  let prevCompactProof: Uint8Array[] = [];
  let prevRoot: Uint8Array;
  let prevIndex: number = 0;
  let prevLeafHash: Uint8Array = Uint8Array.from([...hashes[prevIndex]]);
  {
      let tree = new MerkleTree(hashes.slice(0, prevIndex + 1), hash, {
        hashLeaves: false,
        duplicateOdd: true,
      });

      const proof = tree.getProof(hashes[prevIndex]);

      prevProof = proof.map(val => Uint8Array.from([...val.data]));
      prevCompactProof = ToCompactProof(prevProof, prevIndex);

      prevRoot = tree.getRoot();
  }

  for (prevIndex = 0; prevIndex < 129; prevIndex++) {
      const newCompactProof: Array<Uint8Array> = GetNewCompactProof(prevLeafHash, prevCompactProof, prevIndex);
      const newIndex: number = prevIndex + 1;

      let tree = new MerkleTree(hashes.slice(0, newIndex + 1), hash, {
        hashLeaves: false,
        duplicateOdd: true,
      });

      const proof = tree.getProof(hashes[newIndex]).map(val => Uint8Array.from([...val.data]));

      const { fullProof: newFullProof, root: newRootOut } = ToFullProof(hashes[newIndex], newCompactProof, newIndex);
      const newRoot: Uint8Array = tree.getRoot();
      // verification
      {
          expect(newCompactProof).toEqual(proof);
          expect(binsAreEqual(newRootOut, newRoot)).toBe(true);
          prevProof = newFullProof;
          prevCompactProof = newCompactProof;
          prevRoot = newRoot;
          prevLeafHash = Uint8Array.from([...hashes[newIndex]]);
      }
  }
});

it.skip('print compact proofs', () => {
  for (let i = 2; i < 67; i++) {
    const copy = leaves.slice(0, i + 1);
    let tree = new MerkleTree(copy, hash, {
      hashLeaves: true,
      duplicateOdd: true,
    });
    for (let index = 0; index <= i; index++) {
      const proof = tree.getProof(hashes[index]);
      if (proof.length === ulog2(copy.length - 1)+1) {
        console.log(copy.length, " index: ", index, " cmpct size: ", proof.length, " ulog2: ", ulog2(copy.length - 1)+1);
      }
    }
  }
});



