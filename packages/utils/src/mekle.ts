import { hash160 } from '@bitauth/libauth';

/*
 * Get nearest rounded down log2 base using only bitwise operations
 */
export const ulog2 = (u: number): number => {
  let s: number;
  let t: number;

  // eslint-disable-next-line
  t = Number(u > 0xffff) << 4; u >>= t;
  // eslint-disable-next-line
  s = Number(u > 0xff) << 3; u >>= s, t |= s;
  // eslint-disable-next-line
  s = Number(u > 0xf) << 2; u >>= s, t |= s;
  // eslint-disable-next-line
  s = Number(u > 0x3) << 1; u >>= s, t |= s;

  return (t | (u >> 1));
};

/*
 * Transform a full proof of last element in the data set into a compact proof
 */
export const ToCompactProof = (fullProof: Array<Uint8Array>, elementIndex: number): Array<Uint8Array> => {
  let index: number = 0;
  while (elementIndex) {
    if (elementIndex & 1) {
      index += 1;
    } else {
      fullProof.splice(index, 1);
    }

    elementIndex >>= 1;
  }

  return fullProof;
};

/*
 * Transform the compact proof of last element in the data set into a full proof.
 * This involves computation of the hashes of duplicated (right hand sided) merkle tree elements
 *   to fill the gaps in the sparse compact proof
 * Optionally this method also returns the root of the merkle tree in question
 */
export const ToFullProof = (leafHash: Uint8Array, compactProof: Array<Uint8Array>, elementIndex: number):
{ fullProof: Array<Uint8Array>, root: Uint8Array } => {
  let hash = leafHash; // previous element hash
  let index: number = 0;
  const result: Array<Uint8Array> = [];
  let size: number = 0;
  while (elementIndex) {
    if (elementIndex & 1) {
      // simply copy from merkle branch and compute this level's hash
      result.push(compactProof[index]);
      hash = hash160(Uint8Array.from([...compactProof[index], ...hash]));
      index += 1;
    } else {
      if (!size) {
        // first element being leaf hash
        result.push(leafHash);
      } else {
        result.push(hash);
      }

      // compute missing hash from duplicated items
      hash = hash160(Uint8Array.from([...hash, ...hash]));
    }

    size += 1;
    elementIndex >>= 1;
  }

  return { fullProof: result, root: hash };
};

/*
 * Get the new compact proof given the old compact proof info - sparse proof, previous element hash and previous element index
 *
 * This method is the key for a zero-knowledge expansion of a data set given its previous state.
 * Contracts can use it to permissionlessly and trustlessly propose the next state of a decentralized dataset.
 */
export const GetNewCompactProof = (prevLeafHash: Uint8Array, prevCompactProof: Array<Uint8Array>, prevIndex: number):
Array<Uint8Array> => {
  const newIndex: number = prevIndex + 1; // aka binaryPath
  let setBits: number = ((newIndex - 1) ^ (newIndex)) & (newIndex);

  let newCompactProof: Array<Uint8Array> = [];
  if (setBits === 1) {
    newCompactProof = [prevLeafHash, ...prevCompactProof];
  } else if (setBits === (1 << ulog2(prevIndex))) {
    newCompactProof = [ComputeMerkleRootFromBranch(prevLeafHash, prevCompactProof, 0xffffffff)];
  } else {
    let level: number = 0;
    while ((setBits & 1) === 0) {
      level += 1;
      setBits >>= 1;
    }

    const lowPart = prevCompactProof.slice(0, level);
    const highPart = prevCompactProof.slice(level);

    const subRoot: Uint8Array = ComputeMerkleRootFromBranch(prevLeafHash, lowPart, 0xffffffff);
    newCompactProof = [subRoot, ...highPart];
  }

  return newCompactProof;
};

/* To verify a merkle proof, pass the hash of the element in "leaf", the merkle proof in "branch", and the zero-based
index specifying where the element was in the array when the merkle proof was created.
*/
export const ComputeMerkleRootFromBranch = (leaf: Uint8Array, merkleBranch: Array<Uint8Array>, nIndex: number): Uint8Array => {
  let hash: Uint8Array = leaf;
  for (const element of merkleBranch) {
    if (nIndex & 1) {
      hash = hash160(Uint8Array.from([...element, ...hash]));
    } else {
      hash = hash160(Uint8Array.from([...hash, ...element]));
    }
    nIndex >>= 1;
  }
  return hash;
};
