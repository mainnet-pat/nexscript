import { hash160, hexToBin, binToHex } from '@bitauth/libauth';
import { MerkleTree } from 'merkletreejs';
import fs, { PathLike } from 'fs';
import { asmToBytecode } from './script.js';
import { ToFullProof, ulog2 } from './mekle.js';

type Argument = bigint | boolean | string | Uint8Array;

export interface AbiInput {
  name: string;
  type: string;
  visible?: boolean;
  unused?: boolean;
}

export interface AbiFunction {
  name: string;
  covenant?: boolean;
  inputs: AbiInput[];
}

export interface SourceArtifact {
  contracts: ContractArtifact[];
  source: string;
  compiler: {
    name: string;
    version: string;
  }
  updatedAt: string;
}

export interface ContractArtifact {
  contractName: string;
  constructorInputs: AbiInput[];
  abi: AbiFunction[];
  dependencies?: string[]; // contract names which are referenced by this contract
  bytecode: string;
  contracts: ContractArtifact[]; // nested contracts
}

export interface ContractDependency {
  constructorInputs: AbiInput[];
  constructorArgs: Argument[];
}

export interface ContractCreationParams {
  constraintHash: Uint8Array;
  visibleArgs: Uint8Array;
  lockingBytecode: Uint8Array;
}

export function importArtifact(artifactFile: PathLike): SourceArtifact {
  return JSON.parse(fs.readFileSync(artifactFile, { encoding: 'utf-8' }));
}

export function exportArtifact(artifact: SourceArtifact, targetFile: string): void {
  const jsonString = JSON.stringify(artifact, null, 2);
  fs.writeFileSync(targetFile, jsonString);
}

interface McpParams {
  artifact: ContractArtifact,
  parameterValues: Argument[],
}

interface MerkleParams {
  root: string,
  path: string[],
  pathBin: string, // 0010
  pathInt: bigint, // 2n
}

const getMerkleParams = (artifact: ContractArtifact, contracts: ContractArtifact[]): MerkleParams => {
  const leaves = contracts.map((val) => Buffer.from(asmToBytecode(val.bytecode)));
  const lookingForLeaf = Buffer.from(hash160(asmToBytecode(artifact.bytecode)));

  const hash = (value: string | Buffer): Buffer => Buffer.from(
    hash160(typeof value === 'string' ? hexToBin(value as string) : value as any),
  );
  const tree = new MerkleTree(leaves, hash, {
    hashLeaves: true,
    duplicateOdd: true,
  });

  let proof = tree.getProof(lookingForLeaf);

  if (proof.length < ulog2(leaves.length - 1) + 1) {
    const index = contracts.findIndex((contract) => contract.bytecode === artifact.bytecode);
    const { fullProof } = ToFullProof(
      Uint8Array.from([...lookingForLeaf]),
      proof.map((val) => Uint8Array.from([...val.data])),
      index,
    );
    const positioned = fullProof.map((fullProofElement) => ({
      position: proof.find((val) => val.data.equals(Buffer.from(fullProofElement)))?.position ?? 'left',
      data: Buffer.from(fullProofElement),
    }));

    proof = positioned;
  }

  proof[0].position = proof[0].position === 'right' ? 'left' : 'right';
  const merklePathBin = proof.map((val) => (val.position === 'right' ? '0' : '1')).reverse().join('');
  const merklePathInt = BigInt(`0b${merklePathBin}`);

  const merklePath = proof.map((val) => val.data.toString('hex'));

  return {
    root: tree.getHexRoot().slice(2),
    path: merklePath,
    pathBin: merklePathBin,
    pathInt: merklePathInt,
  };
};

const prepareMcpContractSingleScript = (
  functionName: string | undefined,
  mastArtifact: ContractArtifact,
  mcpArtifact: ContractArtifact,
): McpParams => {
  const abiFunction = functionName ? mastArtifact.abi.find((val) => val.name === functionName) : mastArtifact.abi[0];
  if (!abiFunction) {
    throw Error(`Function with name '${functionName}' not found in '${mastArtifact.contractName}' artifact`);
  }

  mcpArtifact.abi = [
    {
      name: 'spend',
      inputs: [
        {
          name: 'SS', // subscript
          type: 'bytes',
        },
      ],
    },
  ];

  mcpArtifact.abi[0].inputs.push(
    ...abiFunction.inputs.slice().reverse(),
  );

  if (mastArtifact.abi.length > 1) {
    mcpArtifact.abi[0].inputs.push({
      name: 'FunctionIndex',
      type: 'int',
    });
  }

  mcpArtifact.abi[0].inputs.push(
    ...mcpArtifact.constructorInputs.slice().reverse(),
  );

  mcpArtifact.abi[0].inputs.push({
    name: 'DUMMY',
    type: 'int',
  });

  mcpArtifact.bytecode = `
OP_DUP
OP_HASH160
${binToHex(hash160(asmToBytecode(mastArtifact.bytecode)))}
OP_EQUALVERIFY

OP_1NEGATE
OP_PLACE
OP_DROP

OP_${mcpArtifact.abi[0].inputs.length - 2}
OP_0
OP_EXEC
`;

  return {
    artifact: mcpArtifact,
    parameterValues: [
      asmToBytecode(mastArtifact.bytecode), // SS
    ] as Argument[],
  };
};

export const prepareMcpContract = (
  contractName: string,
  functionName: string | undefined,
  mcpArtifact: ContractArtifact,
): McpParams => {
  const mastArtifact = mcpArtifact.contracts.find((val) => val.contractName === contractName);
  if (!mastArtifact) {
    throw Error(`Contract with name '${contractName}' not found in MAST artifact`);
  }

  if (mcpArtifact.contracts.length === 1) {
    return prepareMcpContractSingleScript(functionName, mcpArtifact.contracts[0], mcpArtifact);
  }

  const abiFunction = functionName ? mastArtifact.abi.find((val) => val.name === functionName) : mastArtifact.abi[0];
  if (!abiFunction) {
    throw Error(`Function with name '${functionName}' not found in '${contractName}' artifact`);
  }

  const merkleParams = getMerkleParams(mastArtifact, mcpArtifact.contracts);
  if (merkleParams.path.length > 19) {
    throw Error('Merkle tree too deep, depth must be less that 19');
  }

  mcpArtifact.abi = [
    {
      name: 'spend',
      inputs: [
        {
          name: 'SS', // subscript
          type: 'bytes',
        },
        {
          name: 'MP', // binary merkle path from leaf to root
          type: 'int',
        },
      ],
    },
  ];

  merkleParams.path.forEach((_, index) => {
    mcpArtifact.abi[0].inputs.push({
      name: `layer_${index}`,
      type: 'bytes',
    });
  });

  mcpArtifact.abi[0].inputs.push(
    ...abiFunction.inputs.slice().reverse(),
  );

  if (mastArtifact.abi.length > 1) {
    mcpArtifact.abi[0].inputs.push({
      name: 'FunctionIndex',
      type: 'int',
    });
  }

  mcpArtifact.abi[0].inputs.push(
    ...mcpArtifact.constructorInputs.slice().reverse(),
  );

  mcpArtifact.abi[0].inputs.push({
    name: 'DUMMY',
    type: 'int',
  });

  mcpArtifact.bytecode = `
OP_DUP
OP_TOALTSTACK
OP_HASH160
OP_TOALTSTACK

766b5297647c687ea9c17c6c52967c

OP_SWAP
OP_ROT
OP_FROMALTSTACK

OP_3
OP_3
OP_EXEC

${[...Array(merkleParams.path.length - 1).keys()].map(() => `
OP_3 OP_ROLL
OP_3
OP_3
OP_EXEC
`).join(' ')}

OP_NIP OP_NIP
${merkleParams.root}
OP_EQUALVERIFY
OP_FROMALTSTACK

OP_1NEGATE
OP_PLACE
OP_DROP

OP_DEPTH
OP_1SUB

OP_0
OP_EXEC
`;

  return {
    artifact: mcpArtifact,
    parameterValues: [
      binToHex(asmToBytecode(mastArtifact.bytecode)), // SS
      merkleParams.pathInt, // MP
      ...merkleParams.path, // rest of params - merkle branches
      // rest needs to be supplied by spender
    ] as Argument[],
  };
};
