pragma nexscript ^0.1.0;

// This is an experimental contract for a more "streaming" Mecenas experience
// Completely untested, just a concept
contract Mecenas(
    bytes8 initialBlock,
) {
    function receive() {
        // configure these constants
        bytes20 constant recipient = 0x;
        bytes20 constant funder = 0x;
        int constant pledgePerBlock = 0;

        // Check that the first output sends to the recipient
        bytes23 recipientLockingBytecode = new LockingBytecodeP2PKT(recipient);
        require(tx.outputs[0].lockingBytecode == recipientLockingBytecode);

        // Check that time has passed and that time locks are enabled
        int initial = int(initialBlock);
        require(tx.time >= initial);

        // Calculate the amount that has accrued since last claim
        int passedBlocks = tx.locktime - initial;
        int pledge = passedBlocks * pledgePerBlock;

        // Calculate the leftover amount
        int minerFee = 1000;
        int currentValue = tx.inputs[this.activeInputIndex].value;
        int changeValue = currentValue - pledge - minerFee;

        // If there is not enough left for *another* block after this one,
        // we send the remainder to the recipient. Otherwise we send the
        // remainder to the recipient and the change back to the contract
        if (changeValue <= pledgePerBlock + minerFee) {
            require(tx.outputs[0].value == currentValue - minerFee);
        } else {
            // Check that the outputs send the correct amounts
            require(tx.outputs[0].value == pledge);
            require(tx.outputs[1].value == changeValue);

            // Extract the template hash from the lockingbytecode
            bytes templateHash = hash160(this.activeBytecode);

            // Create the new constraintScript
            bytes newConstrainScript = 0x08 + bytes8(tx.locktime);
            bytes20 constraintHash = hash160(newConstrainScript);

            // Create the locking bytecode for the new contract and check that
            // the change output sends to that contract
            bytes newContractLock = new LockingBytecodeP2ST(templateHash, constraintHash, 0x);
            require(tx.outputs[1].lockingBytecode == newContractLock);
        }
    }

    function reclaim(pubkey pk, sig s) {
        require(hash160(pk) == funder);
        require(checkSig(s, pk));
    }
}
